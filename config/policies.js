/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

    /***************************************************************************
     *                                                                          *
     * Default policy for all controllers and actions (`true` allows public     *
     * access)                                                                  *
     *                                                                          *
     ***************************************************************************/

    '*': true,

    /***************************************************************************
     *                                                                          *
     * Here's an example of mapping some policies to run before a controller    *
     * and its actions                                                          *
     *                                                                          *
     ***************************************************************************/
    AdminController: {
        '*': false,
        user: ['isAuthenticated', 'isAdmin'],
        profil: ['isAuthenticated'],
        team: ['isAuthenticated', 'isAdmin'],
        dashboard: ['isAuthenticated'],
        userSearch: ['isAuthenticated', 'isAdmin']

    },
    EvaluationController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isTeacher']
    },
    ExerciceController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isTeacher']
    },
    ExerciceUserController: {
        '*': ['isAuthenticated'],
        exerciceUser: ['isAuthenticated', 'isStudent']
    },
    KitController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isAdmin']
    },
    LocationController: {
        '*': ['isAuthenticated']
    },
    MailerController: {
        '*': ['isAuthenticated']
    },
    MaterielController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isAdmin']
    },
    RoleController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isAdmin']
    },
    TeamController: {
        '*': ['isAuthenticated']
    },
    TeamUserController: {
        '*': ['isAuthenticated']
    },
    TypeExerciceController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isAdmin']
    },
    TypeKitController: {
        '*': ['isAuthenticated'],
        dashboard: ['isAuthenticated', 'isAdmin']
    },
    UserController: {
        '*': ['isAuthenticated'],
        register: true,
        forgotPassword: true,
        forgotPasswordSend: true,
        changePassword: true,
        create: true
    },
    UserLevelController: {
        '*': ['isAuthenticated']
    }


};
