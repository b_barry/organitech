/**
 * AuthController
 *
 * @description :: Server-side logic for managing Authcontrollers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');

module.exports = {



    /**
     * `AuthController.login()`
     */
    login: function (req, res) {
        res.locals.flash = _.clone(req.session.flash);
        req.session.flash = {};
        return res.view();
    },


    /**
     * `AuthController.proccessLogin()`
     **/
    proccessLogin: function (req, res) {

        passport.authenticate('local', function (err, user, info) {
            res.locals.flash = _.clone(req.session.flash);
            req.session.flash = {};
            console.log(info);
            if (err) {
                console.log('error  ds l auth');
                return res.redirect('/auth/login');
            }
            if (!user) {
                req.flash('alert', ["danger", info.message]);
                console.log('user not found');
                return res.redirect('/auth/login');
            }
            if (user) {
                req.logIn(user, function (err) {
                    if (err) {
                        req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                        return res.redirect('/auth/login');
                    }
                    console.log('redirect dash');
                    console.log('user found ' + user.email);
                    return res.redirect('/dashboard')

                });

            }

        })(req, res);

    },
    activationAccount: function (req, res) {
        var token = req.params.token,
            role = req.params.role;

        /*
         TODO: TimeOut for Token
         */
        User.findOne({ token: token }).exec(function (error, user) {
            if (error) {
                req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                return res.redirect('users/register');
            }
            if (user) {
                User.update({token: token}, {activate: true, token: '111-222-333'}).exec(function afterwards(err, updated) {
                    if (err) {
                        req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                        return res.redirect('users/register');
                    }
                    else {
                        console.log("USER Updated", updated[0]);
                        if (ToolsService.compareHash("teacher", role)) {
                            MailerService.sendActivationAccountTeacher(updated[0], function (data) {
                                console.log("##### SENDED ACTIVATION TEACHER #####")
                                console.log(data);
                            });
                        }
                        req.flash('alert', ["success", "Votre compte a bien été activé, veuillez vous connecter"]);
                        return res.redirect('/auth/login');
                    }

                });

            }
            else {
                req.flash('alert', ["danger", "Votre token de validation est invalide"]);
                return res.redirect('users/register');
            }
        });
    },
    /**
     * `AuthControllerController.logout()`
     */
    logout: function (req, res) {
        req.logout();
        return res.redirect('/auth/login');
    }
};
