/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admins
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {



    /**
     * `AdminController.user()`
     */
    user: function (req, res) {

        return res.view('backend/admin/user');
    },
    profil: function (req, res) {

        return res.view('backend/admin/profile');
    },
    team: function (req, res) {
        return res.view('backend/admin/team');
    },
    dashboard: function (req, res) {
        return res.view('backend/admin/dashboard-calendar');
    },
    userSearch: function (req, res) {

        return res.view('backend/admin/userSearch');
    }

};

