/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var uuid = require('node-uuid');

module.exports = {

    'register': function (req, res) {
        res.locals.flash = _.clone(req.session.flash);
        res.view();
        req.session.flash = {};
    },

    'forgotPassword': function (req, res) {
        res.locals.flash = _.clone(req.session.flash);
        res.view();
        req.session.flash = {};
    },

    'forgotPasswordSend': function (req, res) {

        var token = ToolsService.getTokenForgotPassword();
        var email = req.body.email;
        User.findOne({ email: email }).exec(function (error, user) {
            if (error) {
                req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                return res.redirect('/auth/forgot-password/');
            }
            if (!user) {
                req.flash('alert', ["success", "Votre adresse émail est invalide"]);
                return res.redirect('/auth/forgot-password/');
            }
            else {
                user.token = token;
                User.update({email: email}, {token: token}).exec(function afterwards(err, updated) {
                    if (err) {
                        req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                        return res.redirect('/auth/forgot-password/');
                    }
                });
                req.flash('alert', ["success", "Un émail vous a été envoyé"]);
                MailerService.sendEmailPasswordReset(email, token);
            }
            return res.redirect('/auth/forgot-password/');
        });
    },

    'changePassword': function (req, res) {
        var email = req.params.email;
        var token = req.params.token;

        User.findOne({ email: email }).exec(function (error, user) {
            if (user) {
                if (token == user.token && token != '111-222-333') {
                    var newPwd = uuid.v1().substring(1, 6);
                    User.update({email: email}, {password: newPwd, token: '111-222-333'}).exec(function afterwards(err, updated) {
                        if (err) {
                            req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                            return res.redirect('/auth/forgot-password/');
                        }
                    });
                    req.flash('alert', ["success", "Votre nouveau mot de passe a été envoyé sur votre émail"]);
                    console.log(email);
                    MailerService.sendNewPassword(email, newPwd);
                    return res.redirect('/auth/forgot-password/');
                }
                else {
                    req.flash('alert', ["warning", "Votre token de validation est invalide"]);
                    return res.redirect('/auth/forgot-password/');
                }
            }
            else {
                req.flash('alert', ["success", "Votre adresse émail est invalide"]);
                return res.redirect('/auth/forgot-password/');
            }
        });
    },

    create: function (req, res) {
        var levelRoleAsked = ToolsService.generateHash(req.param('radioLevel'));
        var newUser = req.body;
        newUser.token = ToolsService.getTokenActivationUser();
        var email = newUser.email;

        if (email.indexOf("@helb-prigogine.be") == -1) {
            req.flash('alert', ["danger", "Votre adresse émail ne corresponds pas à une adresse '@helb-prigogine.be'"]);
            return res.redirect('/auth/sign-up/');
        }

        User.findOne({ email: email }).exec(function (error, user) {
            if (error) {
                req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                return res.redirect('/auth/sign-up/');
            }
            if (user) {
                req.flash('alert', ["info", " L'adresse émail est déjà utilisé"]);
                return res.redirect('/auth/sign-up/');
            }
            else {
                UserLevel.findOne({name: 'student'}).exec(function findOneCB(err, role) {
                    if (err) {
                        req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                        return res.redirect('/auth/sign-up/');
                    } else {
                        newUser.userlevel = role.id;
                        User.create(newUser).exec(function userCreated(err, user) {
                            if (err) {
                                req.flash('alert', ["warning", "Une erreur inconnue s'est produite"]);
                                return res.redirect('/auth/sign-up/');
                            }
                            else {
                                MailerService.sendActivationAccount(newUser.email, newUser.token, levelRoleAsked, function (data) {
                                    console.log(data)
                                });

                                req.flash('alert', ["success", "Un émail d'activation vous a été envoyé"]);
                                return res.redirect('/auth/sign-up/');
                            }
                        });
                    }
                });

            }
        });
    },
    /**
     *  Custom API Method
     *
     */
    /**
     * `AuthControllerController.logedUser()`
     *
     * return the current logged user
     */
    logged: function (req, res) {

        if (req.isAuthenticated()) {
            return res.json(req.user);
        }
        else {
            return res.json({});
        }

    },
    getAllWithoutCurentLogged:function(req,res){
        User.find({
            email: { '!' : [req.user.email] }
        }).populateAll().exec(function afterwards(err, users) {
            if(err){
                return res.json(err);
            }else{
                return res.json(users);
            }
        });
    },
    lock: function (req, res) {
        var idRecord = req.params.id;
        var userId = req.user.id;

        User.update({lock: -1, id: idRecord}, {lock: userId}).exec(function afterwards(err, userUpdated) {
            //TODO:do switch
            console.log(userUpdated);

            if (err) {
                // handle error here- e.g. `res.serverError(err);`
                return res.json(false);
            }
            else {
                User.findOne({id: idRecord}).exec(function findOneCB(err, found) {
                    if (err || _.isEmpty(found)) {
                        // handle error here- e.g. `res.serverError(err);`
                        return res.json(false);
                    }
                    else {

                        if (found.lock == userId) {
                            return res.json(true);
                        }
                        else {
                            return res.json(false);
                        }
                    }
                });
            }
        });

    },
    unlock: function (req, res) {
        var idRecord = req.params.id;
        console.log(req.user.id);
        User.update({lock: req.user.id, id: idRecord}, {lock: -1}).exec(function afterwards(err, userUpdated) {
            if (err) {
                return res.json(false);
            }
            return res.json(true);
        });

    }
};

