/**
 * RoleController
 *
 * @description :: Server-side logic for managing Roles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    /*
     (http://localhost:1337/role/dashboard)
     */
    dashboard: function (req, res) {

        return res.view('backend/role/role');
    },

    getRolesOnly: function (req, res) {
        Role.find().exec(function findCB(err, found) {
            return res.json(found);
        });
    }
};

