module.exports = {
    //(http://localhost:1337/exerciceUsers/exerciceUser)
    exerciceUser: function (req, res) {

        return res.view('backend/exercice/exerciceUser');
    },


    currentUser: function (req, res) {

        return res.json(req.user);
    }

};