/**
 * ExerciceController
 *
 * @description :: Server-side logic for managing Exercices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    /*
     (http://localhost:1337/exercices/dashboard)
     */
    dashboard: function (req, res) {

        return res.view('backend/exercice/exercice');
    },

    /**
     *  API Method
     *
     */
    indexWithTeamPopulated: function (req, res) {
        var sort = req.query.sort ? req.query.sort : 'createdAt DESC';

        Exercice.find({sort: sort}).populateAll().exec(function (err, results) {
            var exercices = _.cloneDeep(results);

            async.eachSeries(exercices, function (exercice, callback) {
                var tabTeamId = _.pluck(exercice.team, 'id');

                Team.find({id: tabTeamId}).populate('team_user').populate('evaluation').exec(function (err, teams) {
                    if (err) {
                        console.log('error ds team find');
                        callback(err);
                    }
                    console.log("Teams Recuperer")
                    exercice.team = teams;
                    callback();
                });
            }, function (err) {
                if (err) {
                    console.log('error merde');
                }
                return res.json(exercices);
            });
        });

    },
    indexWithTeamPopulatedPaginate: function (req, res) {
        var page = req.query.page ? req.query.page : 1,
            limit = req.query.limit ? req.query.limit : 10,
            sort = req.query.sort ? req.query.sort : 'createdAt ASC';

        Exercice.find({sort: sort}).paginate({page: page, limit: limit}).populateAll().exec(function (err, results) {
            var exercices = _.cloneDeep(results);

            async.each(exercices, function (exercice, callback) {

                var tabTeamId = _.pluck(exercice.team, 'id');

                Team.find({id: tabTeamId}).populate('team_user').exec(function (err, teams) {
                    if (err) {
                        console.log('error ds team find');
                        callback(err);
                    }
                    console.log("Teams Recuperer");
                    exercice.team = teams;

                    async.each(exercice.team, function (team, callback) {

                        var tabTeamUserId = _.pluck(team.team_user, 'id');

                        TeamUser.find({id: tabTeamUserId}).populate('user').populate('roles').sort('user ASC').sort('chef ASC').exec(function (err, teamUsers) {
                            if (err) {
                                console.log('error ds team user find');
                                callback(err);
                            }
                            console.log("Teams User Recuperer");
                            team.team_user = teamUsers;

                            callback();

                        });
                    }, function (err) {
                        if (err) {
                            console.log('error merde');
                            callback(err);
                        }
                        callback();
                    });

                });
            }, function (err) {
                if (err) {
                    console.log('error merde');
                }
                return res.json(exercices);
            });
        });
    },
    calendar: function (req, res) {
        var dataCalendars = [],
            exerciceCalendar = {},
            sort = 'createdAt ASC';

        Exercice.find({sort: sort}).populateAll().exec(function (err, results) {
            var exercices = _.cloneDeep(results);
            /*
             console.log(exercices);
             */
            async.each(exercices, function (exercice, callback) {
                if (!_.isEmpty(exercice) && !_.isUndefined(exercice) && !_.isNull(exercice)) {
                    exerciceCalendar.title = exercice.name + ' :\n\n' + exercice.description + '\n\n crée par ' + exercice.author.first_name + ' ' + exercice.author.last_name;
                    exerciceCalendar.start = exercice.date_begin;
                    exerciceCalendar.end = exercice.date_end;
                    exerciceCalendar.allDay = false;
                    exerciceCalendar.author = exercice.author;
                    exerciceCalendar.id = exercice.id;
                    exerciceCalendar.roles = exercice.roles;
                    exerciceCalendar.locations = exercice.locations;
                    exerciceCalendar.typeKits = exercice.typeKits;
                    exerciceCalendar.color = exercice.type.couleur;
                    exerciceCalendar.team = _.pluck(exercice.team, 'id');

                }


                dataCalendars.push(exerciceCalendar);
                exerciceCalendar = null;
                exerciceCalendar = {};
                callback();

            }, function (err) {
                if (err) {
                    console.log('error merde generation data calendrier');
                }
                return res.json(dataCalendars);
            });


        });
    }
};
