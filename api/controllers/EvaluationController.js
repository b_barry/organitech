/**
 * ExerciceController
 *
 * @description :: Server-side logic for managing Kits
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    dashboard: function (req, res) {

        return res.view('backend/exercice/evaluation');
    }
};

