/**
 * MailController
 *
 * @description :: Server-side logic for managing mails
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    sendActivationUserByAdmin: function (req, res) {
        var userAddress = req.query.userAddress ,
            name = req.query.username;
        if (!_.isEmpty(userAddress) && !_.isEmpty(name)) {

            MailerService.sendActivationUserByAdmin(userAddress, name, function (data) {
                console.log(data);
                return res.json(data);
            });
        }
        else {
            return res.json("error");
        }

    },
    sendDeactivationUserByAdmin: function (req, res) {
        var userAddress = req.query.userAddress ,
            name = req.query.username;
        if (!_.isEmpty(userAddress) && !_.isEmpty(name)) {
            MailerService.sendDeactivationUserByAdmin(userAddress, name, function (data) {
                console.log(data);
                return res.json(data);
            });
        }
        else {
            return res.json("error");
        }

    },
    sendUpdatedUserRoleByAdmin: function (req, res) {
        var userAddress = req.query.userAddress ,
            name = req.query.username,
            role = req.query.role;
        if (!_.isEmpty(userAddress) && !_.isEmpty(name) && !_.isEmpty(role)) {
            MailerService.sendUpdatedUserRoleByAdmin(userAddress, name, role, function (data) {
                console.log(data);
                return res.json(data);
            });

        }
        else {
            return res.json("error");
        }
    },
    sendTeamAdded: function (req, res) {
        var userAddress = req.query.userAddress ,
            name = req.query.username,
            chefName = req.query.chefName ,
            team = req.query.team ,
            role = req.query.role,
            exercice = req.query.exercice,
            author = req.query.author;
        if (!_.isEmpty(userAddress) && !_.isEmpty(name)
            && !_.isEmpty(chefName) && !_.isEmpty(team)
            && !_.isEmpty(role) && !_.isEmpty(exercice)
            && !_.isEmpty(author)) {
            MailerService.sendTeamAdded(userAddress, name, team, role, exercice, author, chefName, function (data) {
                console.log(data);
                return res.json(data);
            });
        }
        else {
            return res.json("error");
        }
    }
};
