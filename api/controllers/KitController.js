/**
 * ExerciceController
 *
 * @description :: Server-side logic for managing Kits
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    dashboard: function (req, res) {

        return res.view('backend/kit/kit');
    },

    getKitsOnly: function (req, res) {
        Kit.find().exec(function findCB(err, found) {
            return res.json(found);
        });
    }
};

