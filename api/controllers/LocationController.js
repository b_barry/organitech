/**
 * LocationController
 *
 * @description :: Server-side logic for managing locations
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    /**
     *  API Method
     *
     */

    calendar: function (req, res) {
        var dataCalendars = [],
            locationCalendar = {},
            sort = 'createdAt ASC';

        Location.find({sort: sort}).populateAll().exec(function (err, results) {
            var locations = _.cloneDeep(results);
            /*
             console.log(locations);
             */
            async.each(locations, function (location, callback) {
                if (!_.isEmpty(location) && !_.isUndefined(location)) {
                    locationCalendar.title = location.name + ' :\n' + location.description + ' crée par ' + location.author.first_name + ' ' + location.author.last_name;
                    locationCalendar.start = location.date_begin;
                    locationCalendar.end = location.date_end;
                    locationCalendar.allDay = false;
                    locationCalendar.author = location.author;
                    locationCalendar.id = location.id;
                    locationCalendar.roles = location.roles;
                    locationCalendar.locations = location.locations;
                    locationCalendar.typeKits = location.typeKits;
                    locationCalendar.team = _.pluck(location.team, 'id');
                }
                dataCalendars.push(locationCalendar);
                locationCalendar = null;
                locationCalendar = {};
                callback();

            }, function (err) {
                if (err) {
                    console.log('error merde generation data calendrier');
                }
                return res.json(dataCalendars);
            });


        });
    }
};

