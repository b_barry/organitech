/**
 * TypeExerciceController
 *
 * @description :: Server-side logic for managing Typeexercices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var util = require('util');

module.exports = {
    /*
     (http://localhost:1337/TypeExercices/dashboard)
     */
    dashboard: function (req, res) {

        return res.view('backend/type/type_exercice');
    },

    getExerciceTypeOnly: function (req, res) {
        TypeExercice.find().exec(function findCB(err, found) {
            /* var arrayType = [];

             for (i = 0; i < found.length; i++) {
             arrayType[i] = found.pop();
             }
             while (found.length){
             //                var obj_str = util.inspect(found.pop());
             ////                console.log(obj_str);
             //                  arrayType[i] = found.pop();
             ////                console.log("Apple" + found.pop());
             ////            }
             console.log("Apple" + util.inspect(arrayType[0]));
             console.log("Pear" + arrayType[1]);
             */
            return res.json(found);
        });
    }

};

