/**
 * MaterielController
 *
 * @description :: Server-side logic for managing Materiels
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    /*
     (http://localhost:1337/materiels/dashboard)
     */
    dashboard: function (req, res) {

        return res.view('backend/materiel/materiel');
    },

    getMaterielOnly: function (req, res) {
        Materiel.find().exec(function findCB(err, found) {
            return res.json(found);
        });
    }
};

