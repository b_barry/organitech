/**
 * TypeKitController
 *
 * @description :: Server-side logic for managing Typekits
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    /*
     (http://localhost:1337/TypeExercices/dashboard)
     */
    dashboard: function (req, res) {

        return res.view('backend/type/type_kit');
    },

    getTypeKit: function (req, res) {

        TypeKit.find().exec(function findCB(err, found) {

            return res.json(found);

        })
    }

};

