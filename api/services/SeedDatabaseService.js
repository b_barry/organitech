var moment = require('moment');

module.exports = {
    user: function () {
        async.waterfall([
            function createUserLevel(callback) {
                UserLevel.count().exec(function countCB(error, found) {
                    if (found === 0) {
                        async.parallel([
                                function admin(cb) {
                                    UserLevel.create({name: 'admin'}).exec(function createCB(err, created) {
                                        if (err) cb(err);
                                        cb(null, created);
                                    });

                                },
                                function teacher(cb) {
                                    UserLevel.create({name: 'teacher'}).exec(function createCB(err, created) {
                                        if (err) cb(err);
                                        cb(null, created);
                                    });

                                },
                                function student(cb) {
                                    UserLevel.create({name: 'student'}).exec(function createCB(err, created) {
                                        if (err) cb(err);
                                        cb(null, created);
                                    });

                                }
                            ],
                            function (err, results) {
                                if (err) callback(err);
                                callback(null, _.where(results, {name: 'admin'})[0]);
                            });

                    } else {
                        callback(new Error("UserLevel déjà créer"));
                    }
                });

            },
            function createAdminUser(adminRole, callback) {
                User.count().exec(function countCB(error, found) {
                    if (found === 0) {
                        async.parallel([
                                function admin1(cb) {
                                    User.create({email: 'adminUser1@helb-prigogine.be', password: "123456789",
                                        last_name: "adminUser1",
                                        first_name: "One Piece", activate: true, userlevel: adminRole.id}).exec(function createCB(err, created) {
                                        if (err) cb(err);
                                        cb(null, created);
                                    });

                                },
                                function admin2(cb) {
                                    User.create({email: 'adminUser2@helb-prigogine.be', password: "123456789",
                                        last_name: "adminUser2",
                                        first_name: "One Piece", activate: true, userlevel: adminRole.id}).exec(function createCB(err, created) {
                                        if (err) cb(err);
                                        cb(null, created);
                                    });

                                }
                            ],
                            function (err, results) {
                                if (err || results.length !== 2) callback(err);
                                callback(null, true);

                            });

                    } else {
                        callback(new Error("User déjà créer"));
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                console.log("ERROR END WATERFALL ", err);
            }
            else {
                console.log("admin bien Cree")
            }
        });
    },
    typeExercice: function () {
        TypeExercice.count().exec(function countCB(error, found) {
            if (found === 0) {
                async.parallel([
                        function image(cb) {
                            TypeExercice.create({
                                name: "Image"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }

                            });

                        },
                        function montage(cb) {
                            TypeExercice.create({
                                name: "Montage"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function son(cb) {
                            TypeExercice.create({
                                name: "Son"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function ism(cb) {
                            TypeExercice.create({
                                name: "ISM (global)"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function photo(cb) {
                            TypeExercice.create({
                                name: "Photo"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function transmedia(cb) {
                            TypeExercice.create({
                                name: "Transmedia"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function fiction(cb) {
                            TypeExercice.create({
                                name: "Fiction"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function reel(cb) {
                            TypeExercice.create({
                                name: "Réel"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function entrainement(cb) {
                            TypeExercice.create({
                                name: "Entrainement"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }

                    ],
                    function (err, results) {
                        if (err || results.length !== 9) {
                            console.log("TYPE EXERCICE NON CREATE ALL");
                        }
                        else {
                            console.log("TYPE EXERCICE CREE");
                        }


                    });

            } else {
                console.log("TYPE EXERCICE DEJA CREE");
            }
        });
    },
    typeKit: function () {
        TypeKit.count().exec(function countCB(error, found) {
            if (found === 0) {
                async.parallel([
                        function imageVideo(cb) {
                            TypeKit.create({
                                name: "Image Vidéo "
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function fictionTfa(cb) {
                            TypeKit.create({
                                name: "Fiction TFA"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function son(cb) {
                            TypeKit.create({
                                name: "Son"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function photo(cb) {
                            TypeKit.create({
                                name: "Photo"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }

                            });

                        }

                    ],
                    function (err, results) {
                        if (err || results.length !== 4) {
                            console.log("TYPE KIT NON CREATE ALL", err);
                        }
                        else {
                            console.log("TYPE KIT CREE");
                        }


                    });

            } else {
                console.log("TYPE KIT DEJA CREE");
            }
        });
    },
    materiel: function () {
        Materiel.count().exec(function countCB(error, found) {
            if (found === 0) {
                async.parallel([
                        function mat1(cb) {
                            Materiel.create({
                                nom: "Camera VIDEO "
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function mat2(cb) {
                            Materiel.create({
                                nom: "Pieds Cameras VIDEO"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function mat3(cb) {
                            Materiel.create({
                                nom: "Station de montage"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        },
                        function mat4(cb) {
                            Materiel.create({
                                nom: "Appareil PHOTO Nikon D7000"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }, function mat5(cb) {
                            Materiel.create({
                                nom: "Accesoires PHOTO"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }, function mat6(cb) {
                            Materiel.create({
                                nom: "Kit épaule pour Nikom D7000"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }, function mat7(cb) {
                            Materiel.create({
                                nom: "Mixette SON + Zoom SON"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }, function mat8(cb) {
                            Materiel.create({
                                nom: "Perche et micro SON"
                            }).exec(function createCB(err, created) {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    cb(null, created);
                                }
                            });

                        }
                    ],
                    function (err, results) {
                        if (err || results.length !== 2) {
                            console.log("MATERIEL NON CREATE ALL", err);
                        }
                        else {
                            console.log("MATERIEL CREE");
                        }


                    });

            } else {
                console.log("MATERIEL DEJA CREE");
            }
        });
    }
};
