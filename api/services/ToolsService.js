/**
 * Created by Boubacar on 13-10-14.
 */

/**
 * Source : http://blog.tompawlak.org/generate-unique-identifier-nodejs-javascript
 * @type {{}}
 */
var uuid = require('node-uuid'),
    hasher = require('password-hash');
;

module.exports = {
    getTokenActivationUser: function () {
        return uuid.v1();
    },
    getTokenForgotPassword: function () {
        return uuid.v4();
    },
    generateHash: function (data) {
        console.log("entrain de hash");
        return hasher.generate(data);
    },
    compareHash: function (data, hash) {
        return hasher.verify(data, hash);
    },
    isHashed: function (value) {
        return hasher.isHashed(value);
    }

};
