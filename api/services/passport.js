var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findOne({ where: {id: id}}).populateAll().exec(function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, done) {
        User.findOne({ where: {email: email}}).exec(function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, { message: "Votre nom d'utlisateur ou mot de passe est incorrect"});
            }
            if (!ToolsService.compareHash(password, user.password)) {
                return done(null, false, { message: "Votre nom d'utlisateur ou mot de passe est incorrect" });
            }
            if (user.activate != true) {
                return done(null, false, {message: "Veuillez valider votre compte"});
            }
            return done(null, user);
        });
    }
));


//module.exports.http = {
//    customMiddleware: function (app) {
//        app.use(passport.initialize());
//        app.use(passport.session());
//    }
//};
