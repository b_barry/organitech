/**
 * Created by Paulo on 12/1/2014.
 */

var cron = require('cron');
var moment = require('moment');

module.exports = {
    startCron: function () {
        //  relance la function toute les minutes
//        var cronJob = cron.job('* * * * *', function(){
        //relance la function toute les 4 min
        var cronJob = cron.job('*/4 * * * *', function () {
            CronService.unlockUser();
        });
        cronJob.start();
    },

    unlockUser: function (date) {
        var nowMinus4 = moment().subtract(4, 'minute');
        User.update({lock: { '!': -1}, updatedAt: {'<=': new Date(nowMinus4)} }, {lock: -1}).exec(function afterwards(err, updated) {
            if (err) {
                return;
            }
            else {
                console.info('Cron updated ok');
            }

        });


    }
};
