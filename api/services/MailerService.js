/**
 * Created by Boubacar on 14-10-14.
 *  based on the work of Joao Pinto - pinto.joao@outlook.com
 -> https://github.com/JoaoPintoM/node-local-auth-template.git
 */
var nodemailer = require('nodemailer'),
    markdown = require('nodemailer-markdown').markdown;
baseUrl = /*"localhost:1337"*//*sails.getBaseurl()*/"3tiers-bbarry.rhcloud.com",
    emailAdmins = ['thierry.content@gmail.com'];

// create reusable transporter object using SMTP transport
/*var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'organitech.helb@gmail.com',
        pass: 'helbinraci'

    }
});*/
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'sidbouba@gmail.com',
        pass: 'barry130893'

    }
});
transporter.use('compile', markdown());
module.exports = {
    sendActivationAccount: function (userAddress, token, role, callback) {
        var markdown = "Bienvenue sur Organitech, \n\n" +
            "Veuillez cliquer sur le lien ci-dessous pour activer votre compte: \n\n" +
            "[Activation du compte](" + baseUrl + "/auth/activation-account/" + token + "/" + role + ") \n\n" +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receivers
            subject: 'Organitech : [Email Activation]',// Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendActivationAccountTeacher: function (user, callback) {
        var markdown = " Bonjour , \n\n" +
            "Veuillez vous connecter à l'espace d'administration pour accepter une nouvelle demamde de professeur : \n\n" +
            "   N°:" + user.id + "\n" +
            "   Nom et prenom °:" + user.first_name + " " + user.last_name + "\n" +
            "   Email :" + user.email + "\n" +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: emailAdmins, // list of receivers
            subject: 'Organitech : [Email Activation Professeur]',// Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendEmailPasswordReset: function (userAddress, token, callback) {
        var markdown = " Bonjour , \n\n" +
            "Si vous avez effectué une demande de réinitialisation de votre mot de passe actuel," +
            " veuillez cliquer sur le lien suivant : \n\n" +
            "[Réinitialisation mot de passe](" + baseUrl + "/auth/change-password/" + userAddress + "/" + token + ") \n\n" +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receiver
            subject: 'Organitech : [Réinitialisation Mot de passe]', // Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);

    },

    sendNewPassword: function (userAddress, password, callback) {
        var markdown = "Bonjour , \n\n" +
            "Votre mot de passe a bien été réinitialisé. Voici votre nouveau mot de passe " + password + "\n\n" +
            "Vous pourrez le modifier via votre page de profil" +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receiver
            subject: 'Organitech : [Nouveau Mot de passe ]', // Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendActivationUserByAdmin: function (userAddress, name, callback) {
        var markdown = " Bonjour " + name + ", \n\n" +
            "Votre compte a été activé par un administrateur. \n\n " +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receiver
            subject: 'Organitech ', // Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendDeactivationUserByAdmin: function (userAddress, name, callback) {
        var markdown = " Bonjour " + name + ", \n\n" +
            "Votre compte a été désactivé par un administrateur. \n\n " +
            "Veuillez prendre contact avec ces derniers \n\n" +
            "Bien à vous,\n\n" +
            "**Organitech Bots**";

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receiver
            subject: 'Organitech : [Désactivation compte ]', // Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendUpdatedUserRoleByAdmin: function (userAddress, name, role, callback) {
        var markdown = " Bonjour " + name + ", \n\n" +
            "Vos droits d'accés dans Organitech ont été mise à jour \n\n " +
            "Vous êtes maintenant " + role +
            " \n\nBien à vous,\n\n" +
            "**Organitech Bots**";

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receiver
            subject: "Organitech : [Droit d'accés  ]", // Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    },
    sendTeamAdded: function (userAddress, name, team, role, exercice, author, chefName, callback) {
        var markdown = " Bonjour " + name + ", \n\n" +
            "**" + chefName + "** vous a rajouté dans sa ** Team" + team + "** et votre rôle est " + role + " .\n\n" +
            "L'exercice est " + exercice + " a été crée par " + author +
            "\n\n Bien à vous,\n\n" +
            "**Organitech Bots**";
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'Organitech Bot ✔ <organitech.helb@gmail.com>', // sender address
            to: userAddress, // list of receivers
            subject: 'Organitech : [Team ]',// Subject line
            markdown: markdown
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, callback);
    }


};
