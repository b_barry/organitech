module.exports = function isAdmin(req, res, next) {
    var userLevel = _.pluck(req.user.userlevel, 'name'),
        authorizedUserLevel = ['admin'];

    if (_.intersection(userLevel, authorizedUserLevel).length) {
        return next();
    }
    else {
        return res.forbidden("Don't have authorisation ");
    }


};
