module.exports = function isTeacher(req, res, next) {
    var userLevel = _.pluck(req.user.userlevel, 'name'),
        authorizedUserLevel = ['admin', 'teacher'];
    console.log(userLevel);

    if (_.intersection(userLevel, authorizedUserLevel).length) {
        return next();
    }
    else {
        return res.forbidden("Don't have authorisation ");
    }

};
