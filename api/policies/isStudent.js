module.exports = function isStudent(req, res, next) {
    var userLevel = _.pluck(req.user.userlevel, 'name'),
        authorizedUserLevel = ['admin', 'teacher', 'student'];
    console.log(userLevel);
    if (_.intersection(userLevel, authorizedUserLevel).length) {
        return next();
    }
    else {
        return res.forbidden("Don't have authorisation ");
    }
};
