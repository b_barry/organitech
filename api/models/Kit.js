/**
 * Kits.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        description: {
            type: 'string',
            defaultsTo: ''
        },
        numeroSerie: {
            type: 'integer',
            required: true
        },
        type: {
            model: 'TypeKit'
        },
        linesLocation: {
            collection: "LineLocation",
            via: 'kit',
            dominant: true
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};

