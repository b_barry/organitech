/**
 * TeamUser.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {

        user: {
            model: 'User'
        },
        team: {
            model: 'Team'
        },
        roles: {
            model: 'Role'

        },
        chef: {
            type: "boolean",
            defaultsTo: false
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};