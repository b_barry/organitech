/**
 * Exercices.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },

        max_member: {
            type: 'integer',
            required: true
        },
        description: {
            type: 'text',
            required: true
        },
        date_begin: {
            type: 'datetime',
            required: true
        },
        date_end: {
            type: 'datetime',
            required: true
        },
        team: {
            collection: 'Team',
            via: 'exercice'
        },
        typeKits: {
            collection: 'TypeKit',
            via: 'exercices'
        },
        locations: {
            collection: 'Location',
            via: 'exercice'
        },
        roles: {
            collection: 'Role',
            via: 'exercices'
        },
        type: {
            model: 'TypeExercice'
        },
        author: {
            model: 'User'
        },
        max_day: {
            type: 'String'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }


    }
};