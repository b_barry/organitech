/**
 * Locations.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        date_begin: {
            type: 'datetime',
            required: true
        },
        date_end: {
            type: 'string',
            required: true
        },
        user: {
            model: 'User'
        },
        exercice: {
            model: 'Exercice'

        },
        closed: {
            type: "boolean",
            defaultsTo: false
        },
        linesLocation: {
            collection: 'LineLocation',
            via: 'location',
            dominant: true
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};