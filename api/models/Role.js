/**
 * Roles.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        exercices: {
            collection: 'Exercice',
            via: 'roles'
        },
        teams: {
            collection: 'TeamUser',
            via: 'roles'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};

