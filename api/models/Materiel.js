/**
 * Evaluations.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        nom: {
            type: 'String',
            required: true
        },
        picture: {
            type: 'String'
        },
        instructionManuel: {
            type: 'String'
        },
        type: {
            collection: 'TypeKit',
            via: 'materiel'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};

