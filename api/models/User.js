/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        email: {
            type: 'string',
            email: true,
            required: true,
            unique: true
        },
        password: {
            type: 'string',
            required: true
        },
        last_name: {
            type: 'string',
            required: true
        },
        phoneNumber: {
            type: 'string'
        },
        first_name: {
            type: 'string',
            required: true
        },
        token: {
            type: 'string',
            defaultsTo: '111-222-333'
        },
        activate: {
            type: 'boolean',
            defaultsTo: false,
            required: true
        },
        userlevel: {
            collection: 'UserLevel',
            via: 'user',
            dominant: true
        },
        team_user: {
            collection: 'TeamUser',
            via: 'user'

        },
        exercice: {
            collection: 'Exercice',
            via: 'author',
            dominant: true
        },

        evaluation: {
            collection: 'Evaluation',
            via: 'user'
        },
        locations: {
            collection: 'Location',
            via: 'user'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        },
        /*
         function de class such as fullName, isActivate, isChef,isResponsable
         */

        toJSON: function () {
            var obj = this.toObject();
            delete obj.password;
            delete obj._csrf;
            return obj;
        }

    },
    beforeCreate: function (values, cb) {

        values.password = ToolsService.generateHash(values.password);
        /*
         calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
         */
        return cb();

    },
    beforeUpdate: function (values, cb) {

        if (( values.password != undefined || values.password != null) && !ToolsService.isHashed(values.password)) {
            values.password = ToolsService.generateHash(values.password);
        }
        /*
         calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
         */
        return cb();

    }
};
