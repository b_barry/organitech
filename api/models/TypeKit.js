/**
 * TypeKit.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        materiel: {
            collection: 'Materiel',
            via: 'type',
            dominant: true

        },
        exercices: {
            collection: 'Exercice',
            via: 'typeKits'
        },
        kit: {
            collection: 'Kit',
            via: 'type'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        }
    }
};