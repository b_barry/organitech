/**
 * Teams.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        exercice: {
            model: 'Exercice'
        },
        evaluation: {
            collection: 'Evaluation',
            via: 'team'
        },
        team_user: {
            collection: 'TeamUser',
            via: 'team'
        }, lock: {
            type: 'integer',
            defaultsTo: -1
        },
        validate: {
            type: 'boolean',
            defaultsTo: false
        }
    }
};