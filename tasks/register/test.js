/**
 * Created by Boubacar on 13-10-14.
 */

module.exports = function (grunt) {
    grunt.registerTask('test', ['mochaTest']);
    grunt.registerTask('test-coverage', ['mocha_istanbul:coverage']);
}