﻿(function () {
    'use strict';
    angular
        .module('organitech', ['smart-table',
            'ngResource',
            'angular-loading-bar',
            'ui.calendar',
            'ui.bootstrap',
            'mgcrea.ngStrap.datepicker',
            'mgcrea.ngStrap.collapse',
            'validation',
            'validation.rule',
            'infinite-scroll',
            'ui.select',
            'ngSanitize',
            'toaster',
            'ngTouch',
            "angucomplete"]);

})();

