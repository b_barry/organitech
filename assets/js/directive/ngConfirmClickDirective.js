
(function () {
    'use strict';
    angular
        .module('organitech')
        .directive('ngConfirmClick', [
            function () {
                return {
                    link: function (scope, element, attr) {
                        var msg = attr.ngConfirmClick || "Êtes vous sûr ?";
                        var clickAction = attr.confirmedClick;
                        element.bind('click', function (event) {
                            window.swal({
                                    title: "Attention",
                                    text: msg,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Oui",
                                    cancelButtonText: "Annuler"
                                },
                                function(){
                                    scope.$eval(clickAction);
                                });
                        });
                    }
                };
            }]);
})();
