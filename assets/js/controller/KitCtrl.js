
(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("KitCtrl", KitCtrl);

    KitCtrl.$inject = ['Kit', 'TypeKit', 'Token'];

    function KitCtrl(Kit, TypeKit, Token) {
/* jshint validthis: true */

        var vm = this;

        vm.isCreating = false;
        vm.isEmpty = true;
        vm.currentKit = "";
        vm.currentIndex = 0;
        vm.typeKits = [];
        vm.displayedKits= [];

        vm.getTypeKits = getTypeKit;
        vm.switchToCreateState = switchToCreateState;
        vm.createKit = createKit;
        vm.updateKit = updateKit;
        vm.deleteKit = deleteKit;
        vm.changeCurrentKitDisplayed = changeCurrentKitDisplayed;
        vm.changeCurrentTypeKitsDisplayed = changeCurrentTypeKitsDisplayed;


      activate();

        function activate() {
          getTypeKit();
          if(vm.typeKits[0] != null)
          {
            changeCurrentTypeKitsDisplayed(0);
          }
        }

        function getTypeKit() {
            vm.typeKits = TypeKit.query(function (data) {

              if (vm.typeKits.length > 0) {
                changeCurrentTypeKitsDisplayed(0);
                }
              console.log(data);

            });
            return vm.typeKits;
        }


        function switchToCreateState() {
            vm.isCreating = true;
            vm.currentKit = null;
            vm.displayedKits = [];
            vm.isEmpty = false;

        }

        function createKit() {

            var kit = new Kit({
                numeroSerie: vm.currentKit.numeroSerie,
                description: vm.currentKit.description,
                type: vm.currentKit.type

            });
            Token.get(function (data) {

              kit.$save({_csrf: data._csrf}, function (response) {
                    console.log("success create");
                    console.log(vm.currentKit.type.id);
                    vm.typeKits[vm.currentKit.type.id - 1].kit.push(response);
                    vm.currentKit = angular.copy(response);
                    vm.currentIndex = 0;
                    assignCurrentKitType();
                    getTypeKit();
                    changeCurrentTypeKitsDisplayed(vm.currentKit.type.id - 1);
                    vm.isCreating = false;

                }, function (errorResponse) {
                    console.log("error create");
                });
            });
        }

        function updateKit(index) {
          var kit = new Kit({
            numeroSerie: vm.currentKit.numeroSerie,
            description: vm.currentKit.description,
            type: vm.currentKit.type.id,
            id: vm.currentKit.id
          });
            Token.get(function (data) {
                kit.$update({_csrf: data._csrf}, function (response) {
                    assignCurrentKitType();
                    console.log("success");
                    vm.displayedKits[index] = angular.copy(response);
                    getTypeKit();
                }, function (errorResponse) {
                    console.log("error");
                });
            });


        }

        function assignCurrentKitType() {
            if (vm.currentKit.type != null || vm.currentKit.type != undefined) {

                if (vm.currentKit.type.name == undefined) {
                    var temp = vm.typeKits[_.findIndex(vm.typeKits, { 'id': vm.currentKit.type })];
                    vm.currentKit.type = temp;
                } else {
                    var temp = vm.typeKits[_.findIndex(vm.typeKits, { 'name': vm.currentKit.type.name })];
                    vm.currentKit.type = temp;
                }
            }
        }


        function deleteKit(index) {
          var kit = new Kit({
            numeroSerie: vm.displayedKits[index].numeroSerie,
            description: vm.displayedKits[index].description,
            type: vm.displayedKits[index].type,
            id: vm.currentKit.id


          });
            Token.get(function (data) {
                kit.$remove({_csrf: data._csrf}, function (response) {
                    console.log("success");
                    vm.displayedKits.splice(index, 1);
                    if (vm.currentKit == null || vm.currentKit == undefined)
                        vm.currentKit = angular.copy(vm.exercices[0]);
                    if (vm.displayedKits.length == 0)
                        vm.isEmpty = true;
                  getTypeKit();
                }, function (errorResponse) {
                    console.log("error");
                    console.log(errorResponse);
                });
            });
        }



        function changeCurrentTypeKitsDisplayed(type){
          vm.isCreating = false;
          vm.displayedKits = [];
          _.forEach(vm.typeKits[type].kit, function(kitsearch) {
                vm.displayedKits.push(kitsearch);
                console.log("Ajout d'une kit a afficher");
                vm.isEmpty=false;

          });
          if(vm.displayedKits.length != 0 ){
            vm.isEmpty = false;
            var kit = vm.displayedKits[0];
            vm.currentKit = angular.copy(kit);
          }
          else{
            vm.isEmpty = true;
          }


        }


        function changeCurrentKitDisplayed(index) {
          vm.isCreating = false;
          var kit = vm.displayedKits[index];
            vm.currentKit = angular.copy(kit);
            vm.currentIndex = index;
            assignCurrentKitType();
            vm.isCreating = false;

        }


        function nextKit() {
            vm.skip = vm.skip + vm.limit;
            Kit.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                response.forEach(function (elem) {
                    vm.kits.push(elem);
                });
            }, function (errorResponse) {
                console.log("error nextKit");
            });
        }


    }

})();

