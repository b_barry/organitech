(function () {
  'use strict';
  angular
    .module('organitech')
    .controller("EvaluationCtrl", EvaluationCtrl);

  EvaluationCtrl.$inject = ['$injector', 'Exercice', 'Token', 'User', 'Team', 'TeamUser', 'Evaluation', '$templateCache'];


  function EvaluationCtrl($injector, Exercice, Token, User, Team, TeamUser, Evaluation, $templateCache) {
    /* jshint validthis: true */
    var vm = this;

    vm.exercices = [];
    vm.users = [];
    vm.evaluations = [];
    vm.isUserLoaded = false;
    vm.isEvaluationLoaded = false;
    vm.isEmptyExe = true;
    vm.isEmptyEval = true;
    vm.isEmptyTeam = true;
    vm.currentExercice = "";
    vm.currentIndex = 0;
    vm.page = 1;
    vm.limit = 10;


    vm.isEditing = false;
    vm.currentTeams = [];

    vm.currentEvaluations = [];
    vm.currentEvaluation = {};


    vm.getExecices = getExecices;
    vm.getUsers = getUsers;
    vm.getEvaluations = getEvaluations;
    vm.createEvaluation = createEvaluation;
    vm.deleteEvaluation = deleteEvaluation;

    vm.changeCurrentExerciceDisplayed = changeCurrentExerciceDisplayed;
    vm.getCurrentEvaluations = getCurrentEvaluations;


    vm.nextExercices = nextExercices;


    activate();

    function activate() {
      getUsers();
      getExecices();
      getEvaluations();
    }

    function getExecices() {
      vm.exercices = Exercice.getWithTeamPopulatedPaginate({limit: vm.limit, page: vm.page}, function (data) {
        if (data.length != 0) {
          vm.currentExercice = angular.copy(data[0]);
          vm.currentIndex = 0;
          vm.currentTeams = vm.currentExercice.team;
          vm.currentTeams.activePanel = -1;
          vm.isEmptyExe = false;
        }
      });
      return vm.exercices;
    }

    function getUsers() {
      vm.users = User.query(function (data) {
        if (data.length != 0) {
          vm.isUserLoaded = true;
        }
      });
      return vm.users;
    }

    function getEvaluations() {
      vm.evaluations = Evaluation.query(function (data) {
        if (data.length != 0) {
          vm.isEvaluationLoaded = true;
          vm.isEmptyEval = false;
          console.table(data);
        }
      });
      return vm.evaluations;
    }

    function createEvaluation(index) {

      var evaluation = new Evaluation({
        mark: vm.currentEvaluation.mark,
        ponderation: vm.currentEvaluation.ponderation,
        comment: vm.currentEvaluation.comment,
        team: vm.currentTeams[index].id
      });


      Token.get(function (data) {
        if (evaluation.mark)
          evaluation.$save({_csrf: data._csrf}, function (response) {
            console.log("success create");

            vm.evaluations.push(response);
            vm.currentEvaluations.push(response);
            vm.currentEvaluation = {};
            vm.currentIndex = 0;
            vm.isEmptyEval = false;
            vm.isCreating = false;
            getEvaluations();

          }, function (errorResponse) {
            console.log("error create");
          });
      });
    }


    function deleteEvaluation(index) {
      var evaluation = vm.currentEvaluations[index];
      Token.get(function (data) {
        evaluation.$remove({_csrf: data._csrf}, function (response) {
          console.log("success");
          vm.currentEvaluations.splice(index, 1);

          if (vm.currentEvaluations.length == 0)
            vm.isEmptyEval = true;
          getEvaluations();
        }, function (errorResponse) {
          console.log("error");
          console.log(errorResponse);
        });
      });
    }

    function changeCurrentExerciceDisplayed(index) {
      var exercice = vm.exercices[index];
      vm.currentExercice = angular.copy(exercice);
      vm.currentIndex = index;

      vm.currentTeams = vm.currentExercice.team;
      vm.currentTeams.activePanel = -1;
      vm.isEditing = false;
      vm.isDeletingUserInTeam = false;
      vm.isMovingUserFromTeamToOtherTeam = false;
      vm.selectedTeamToDeleteMember = null;
    }


    function getCurrentEvaluations(index) {
      console.log("coucou");
      vm.currentEvaluations = [];
      console.log(vm.evaluations);
      _.forEach(vm.evaluations, function (evaluationTeam) {

        if (evaluationTeam.team.id == vm.currentTeams[index].id) {

          vm.currentEvaluations.push(evaluationTeam);

        }

      });
      console.log("vm.currentEvaluation");
      console.log(vm.currentEvaluation);

    }

    function nextExercices() {
      vm.page++;
      /*Token.get(function (data) {
       Exercice.paginate({limit: vm.limit, page: vm.page}, function (response) {
       if (response.length > 0) {
       response.forEach(function (elem) {
       vm.exercices.push(elem);
       });
       }
       }, function (errorResponse) {
       console.log("error nextExercice");
       });
       });*/
    }
  }
})();
