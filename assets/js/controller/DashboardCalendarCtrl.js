﻿(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("DashboardCalendarCtrl", DashboardCalendarCtrl);

    DashboardCalendarCtrl.$inject = ['User', 'Token', 'Exercice', 'Location','toaster', '$modal'];

    function DashboardCalendarCtrl(User, Token, Exercice, Location,toaster, $modal) {

        /* jshint validthis: true */
        var vm = this;
        vm.exercices = [];
        vm.locations = [];
        vm.exercicesCalendar = [];
        vm.locationsCalendar = [];

        vm.refresh = refresh;
        vm.getExercices = getExercices;
        vm.getLocations = getLocations;
        vm.renderCalender = renderCalendar;
        vm.eventClickOnCalendar = eventClickOnCalendar;

        vm.uiConfigCal = {
            calendar: {
                header: {
                    left: 'agendaDay,agendaWeek,month',
                    center: 'title',
                    right: 'today prevYear prev,next nextYear'
                },
                firstDay: 1,
                buttonText: {
                    today: "Aujourd'hui",
                    day: "Jour",
                    week: "Semaine",
                    month: "Mois"
                },
                titleFormat: {
                    month: 'MMMM yyyy',                             // September 2009
                    week: " d[ yyyy]{ '&#8212;'[ MMM] d  MMMM yyyy}", // Sep 7 - 13 2009
                    day: 'dddd  d MMMM' //
                },
                columnFormat: {
                    month: 'ddd',    // Mon
                    week: 'ddd dd/MM', // Mon 20/07
                    day: 'dddd dd/MM'  // Monday 20/07
                },
                timeFormat: 'H:mm', // uppercase H for 24-hour clock
                allDayText: '',
                axisFormat: 'H:mm',
                eventClick: vm.eventClickOnCalendar
            }
        };


        activate();

        function activate() {
            getExercices();
            //getLocations();
        }

        function refresh() {
            vm.page = 1;
            vm.limit = 10;
            activate();
        }

        function getExercices() {
            Exercice.getExerciceFormattedCalendar(function (data) {
                if (data.length != 0) {
                    console.table(data);
                    vm.exercices = angular.copy(data);
                    vm.exercicesCalendar.push(vm.exercices);
                    $('#calendarExercice').fullCalendar('changeView','agendaWeek');
                }
            });
        }

        function getLocations() {
            Location.getExerciceFormattedCalendar(function (data) {
                if (data.length != 0) {
                    console.table(data);
                    vm.locations = angular.copy(data);
                    vm.locationsCalendar.push(vm.locations);
                }
            });
        }

        function renderCalendar(calendar) {
            /*
             Improve  it :Trick because uiCalendarConfig is null
             */
            if (calendar == 'calendarExercice') {
                $('#calendarExercice').fullCalendar('render');
            }
            if (calendar == 'calendarLocation') {
                $('#calendarLocation').fullCalendar('render');
            }
            /*
             if(uiCalendarConfig.calendars[calendar]){
             uiCalendarConfig.calendars[calendar].fullCalendar('render');
             }
             */
        }

        function eventClickOnCalendar(data, jsEvent, view) {
            console.log("########## EVENT CLICKED #############");
            moment.locale('fr');
            toaster.pop('info',
                    data.author.first_name+" "+data.author.last_name,
                    data.title+"\n\n"+moment(data.start).format("dddd, Do MMMM  YYYY, H:mm:ss ")+"-"+moment(data.end).format("dddd, Do MMMM  YYYY, H:mm:ss "));
            /*console.log(data);
            console.log(jsEvent);
            console.log(view);*/

        }


    }

})();
