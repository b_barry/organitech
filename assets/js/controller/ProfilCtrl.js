﻿(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("ProfilCtrl", ProfilCtrl);

    ProfilCtrl.$inject = ['User', 'Token', 'toaster'];

    function ProfilCtrl(User, Token, toaster) {

        /* jshint validthis: true */
        var vm = this;

        vm.user = {};
        vm.userEdited = {};
        vm.passwordConfirm = "";
        vm.isEditing = false;


        vm.getUserLogged = getUserLogged;
        vm.editProfil = editProfil;
        vm.updateUserCredentials = updateUserCredentials;
        vm.cancelEditing=cancelEditing;

        activate();

        function activate() {
            getUserLogged();
        }

        function getUserLogged() {
            Token.get(function (data) {
                vm.user = User.logged({_csrf: data._csrf}, function (data) {
                    if (data.length != 0) {
                        vm.userEdited = angular.copy(data);
                        vm.userEdited.password = "";
                    }
                });

                return vm.user;

            });
        }

        function editProfil() {
            vm.isEditing = true;
            vm.userEdited = angular.copy(vm.user);
            vm.userEdited.password = "";
        }

        function updateUserCredentials() {
            var user = vm.userEdited;
            if(user.password.length <6 || vm.passwordConfirm.length<6){
                toaster.pop('error', "Le mot de passe doit avoir min 6 caracteres");
            }
            else if(_.isEmpty(vm.passwordConfirm) || vm.passwordConfirm!==user.password){
                toaster.pop('error', "Confirmation du mot passe invalide");
            }
            else {
                Token.get(function (data) {
                    user.$update({_csrf: data._csrf}, function (data) {
                        if (data.length != 0) {
                            vm.user = data;
                            vm.isEditing = false;
                            toaster.pop('success', "Le mot de passe a bien été mise à jour");
                        }
                        else {
                            toaster.pop('error', "Erreur lors de l'update des informations");
                            vm.isEditing = true;

                        }
                    });
                });

            }
        }

        function cancelEditing(){
            vm.isEditing = false;
            vm.userEdited.password = "";
        }
    }
})();