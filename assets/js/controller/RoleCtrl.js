(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("RoleCtrl", RoleCtrl);

    RoleCtrl.$inject = ['$injector', 'Role', 'Token', 'toaster'];


    function RoleCtrl($injector, Role, Token, toaster) {
        /* jshint validthis: true */
        var vm = this;

        vm.roles = [];
        vm.isCreating = false;
        vm.isEmpty = true;

        vm.currentRole = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;

        vm.refresh = refresh;
        vm.getRoles = getRoles;
        vm.switchToCreateState = switchToCreateState;
        vm.createRole = createRole;
        vm.updateRole = updateRole;
        vm.deleteRole = deleteRole;
        vm.changeCurrentRoleDisplayed = changeCurrentRoleDisplayed;
        vm.nextRoles = nextRoles;

        activate();

        function activate() {
            getRoles();
        }

        function refresh() {
            vm.skip = 0;
            vm.limit = 9;
            activate();
        }

        function getRoles() {
            vm.roles = Role.getPaginate({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentRole = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                }
                vm.isLoading = false;
            });
            return vm.roles;
        }

        function switchToCreateState() {
            vm.isCreating = true;
            vm.currentRole = null;
            vm.isEmpty = false;
        }

        function createRole() {
            var role = new Role({
                name: vm.currentRole.name,
                max_member: vm.currentRole.max_member
            });

            Token.get(function (data) {
                role.$save({_csrf: data._csrf}, function (response) {

                    vm.roles.unshift(response);
                    vm.currentRole = angular.copy(response);
                    vm.currentIndex = 0;

                    vm.isCreating = false;
                }, function (errorResponse) {
                    console.log("error create");
                    toaster.pop('error', "Erreur","Erreur de creation");
                });
            });
        }

        function updateRole() {
            var role = vm.currentRole;
            Token.get(function (data) {
                role.$update({_csrf: data._csrf}, function (response) {
                    vm.roles[vm.currentIndex] = angular.copy(response);
                }, function (errorResponse) {
                    console.log("error update");
                    toaster.pop('error', "Erreur","Erreur d'update");

                });
            });
        }


        function deleteRole(index) {
            var role = vm.roles[index];
            Token.get(function (data) {
                role.$remove({_csrf: data._csrf}, function (response) {
                    vm.roles.splice(index, 1);
                    if (vm.currentRole == null || vm.currentRole == undefined)
                        vm.currentRole = angular.copy(vm.roles[0]);
                    if (vm.roles.length == 0)
                        vm.isEmpty = true;

                    switchToCreateState();
                }, function (errorResponse) {
                    console.log("error delete");
                    toaster.pop('error', "Erreur de supression");

                });
            });
        }

        function changeCurrentRoleDisplayed(index) {
            var role = vm.roles[index];
            vm.currentRole = angular.copy(role);
            vm.currentIndex = index;
            vm.isCreating = false;
        }

        function nextRoles() {
            vm.skip = vm.skip + vm.limit;
            Role.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                response.forEach(function (elem) {
                    vm.roles.push(elem);
                });
            }, function (errorResponse) {
                console.log("error nextRole");
            });
        }
    }
})();