﻿(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("AdminUserCtrl", AdminUserCtrl);

    AdminUserCtrl.$inject = ['User', 'Token', 'UserLevel', 'Mailer', '$modal', 'toaster'];

    function AdminUserCtrl(User, Token, UserLevel, Mailer, $modal, toaster) {

        /* jshint validthis: true */
        var vm = this;

        vm.users = [];
        vm.usersDisplayed = [];
        vm.userLevels = [];

        vm.codeChangeRoleUser = '';
        vm.currentUser = '';
        vm.codeConfirmationChangeRoleUser = '';

        vm.refresh = refresh;
        vm.getUsers = getUsers;
        vm.getUserRoles = getUserRoles;
        vm.translateUserLevel = translateUserLevel;
        vm.activateUserAccount = activateUserAccount;


        vm.showModalManageRole = showModalManageRole;


        activate();

        function activate() {
            getUsers();
            getUserRoles();
        }

        function refresh() {

            activate();
        }

        function getUsers() {
            vm.users = User.getAllWithoutCurentLogged(function (data) {
                if (data.length != 0) {
                }

            });

            return vm.users;
        }

        function getUserRoles() {
            vm.userLevels = UserLevel.query(function (data) {
                if (data.length != 0) {
                    vm.selectedUserLevel = data[1];

                }

            });

            return vm.userLevels;
        }

        function translateUserLevel(name) {
            if (name == 'admin') {
                return 'Administrateur';
            }

            if (name == 'teacher') {
                return 'Professeur';
            }

            if (name == 'student') {
                return 'Etudiant';
            }
        }

        function showModalManageRole(user) {
            vm.currentUser = user;
            var firstName = user.first_name, lastName = user.last_name;
            vm.codeConfirmationChangeRoleUser = (firstName.substr(0, 3) + lastName.substr(0, 3)).toLowerCase();

            var modalManageUserRole = $modal.open({
                templateUrl: '../../templates/modalManageUserRole.html',
                controller: 'ModalManageUserRoleCtrl as modalManageUserRole',
                size: 'lg',
                resolve: {
                    user: function () {
                        return vm.currentUser;
                    },
                    userLevels: function () {
                        return vm.userLevels;
                    },
                    codeConfirmationChangeRoleUser: function () {
                        return vm.codeConfirmationChangeRoleUser;
                    }
                }
            });

            modalManageUserRole.result.then(function (user) {
                replaceOldUserWithNewUser(user);
            }, function (res) {

                toaster.pop('error', "Erreur",res);

            });


        }


        function activateUserAccount(index) {
            var user = angular.copy(vm.users[index]);
            User.lock({userId: user.id}, function (isLocked) {
                if (isLocked) {
                    if (user.activate == false) {
                        user.activate = true;
                        Token.get(function (data) {
                            user.$update({_csrf: data._csrf}, function (data) {
                                if (data.length != 0) {
                                    toaster.pop('success', "Activation compte", "L'utilisateur  " + user.first_name + " " + user.last_name + " a bien été activé");
                                    toaster.pop('info', "Email", " Envoie de l'émail d'information");
                                    Mailer.sendActivationUserByAdmin({_csrf: data._csrf,
                                        userAddress: user.email,
                                        username: user.first_name + " " + user.last_name});
                                    vm.users[index].activate = true;

                                }
                                else {
                                    toaster.pop('error', "Erreur","L'utilisateur n'a pas été mise à jour");

                                }

                                User.unlock({userId: user.id}, function (isUnlocked) {
                                });
                            });
                        });
                    }
                    else {
                        console.log("desactivation du compte");
                        user.activate = false;
                        Token.get(function (data) {
                            user.$update({_csrf: data._csrf}, function (data) {
                                if (data.length != 0) {
                                    toaster.pop('success', "Désactivation compte", "L'utilisateur  " + user.first_name + " " + user.last_name + " a bien été désactivé");
                                    toaster.pop('info', "Email", " Envoie de l'émail d'information");
                                    Mailer.sendDeactivationUserByAdmin({_csrf: data._csrf,
                                        userAddress: user.email,
                                        username: user.first_name + " " + user.last_name});

                                    vm.users[index].activate = false;

                                }
                                else {
                                    toaster.pop('error', "Erreur","L'utilisateur n'a pas été mise à jour");
                                }
                                User.unlock({userId: user.id}, function (isUnlocked) {
                                });
                            });
                        });
                    }
                }
                else {
                    toaster.pop('warning', "Attention","Un admin est deja occupé à update ce user, veuillez ressayer ds quelque minute");
                }

            });


        }

        function replaceOldUserWithNewUser(data) {
            vm.users.forEach(function (elem, index) {
                if (elem == vm.currentUser) {
                    vm.users[index] = data;
                }
            });
        }

    }

})();
