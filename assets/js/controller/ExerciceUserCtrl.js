(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("ExerciceUserCtrl", ExerciceUserCtrl);

    ExerciceUserCtrl.$inject = ['Exercice', 'Token', 'Team', 'User', 'TeamUser', 'Role', '$injector'];


    function ExerciceUserCtrl(Exercice, Token, Team, User, TeamUser, Role, $injector) {
        /* jshint validthis: true */
        /***
         *
         * @type {ExerciceUserCtrl}
         */
        var vm = this;
        // exercice
        vm.isEmpty = true;
        vm.exercices = [];
        vm.currentExercice = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;
        vm.currentExerciceRoles = [];
        vm.max_member = 0;
        //teamUser
        vm.isAdding = false;

        //team
        vm.isTeamEmpty = true;
        vm.currentExerciceTeams = [];
        vm.isTeamCreating = false;
        vm.teamNumberAdding = "";
        vm.teamId = -1;
        //UserConnecte
        vm.currentUser = "";
        vm.isUserAvalable = true;
        //Users
        vm.users = [];
        vm.errorMessage = "";
        vm.date = "";
        //UserConnecte
        vm.getUser = getUser;
        vm.getUsers = getUsers;
        //Exercice
        vm.getExecices = getExecices;
        vm.changeCurrentExerciceDisplayed = changeCurrentExerciceDisplayed;
        vm.nextExercices = nextExercices;
        //teamUser
        vm.addMembre = addMembre;
        //team
        vm.switchToCreateState = switchToCreateState;
        vm.switchToAddingState = switchToAddingState;
        vm.createTeamIndividu = createTeamIndividu;
        vm.createTeamGroup = createTeamGroup;
        vm.cancelCreationTeam = cancelCreationTeam;
        vm.cancelAdding = cancelAdding;
        vm.validation = validation;

        // fonction de recherche
        vm.findOneUser = findOneUser;
        vm.findOneUserByEmail = findOneUserByEmail;
        vm.findRole = findRole;
        vm.finsTeam = findTeam;
        vm.currentTime = "";
        activate();


        function activate() {
            getUser();
            getExecices();
            getUsers();
        }


        function getUser() {

            vm.currentUser = User.logged(function (data) {

                vm.currentUser = data;

            });
            return vm.currentUser;
        }

        function getUsers() {
            vm.users = User.getPaginateUser({limit: vm.limit, skip: vm.skip}, function (data) {
            });
            return vm.users;
        }

        function getExecices() {
            vm.exercices = Exercice.getWithTeamPopulatedPaginate({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentExercice = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                    vm.currentExerciceRoles = vm.currentExercice.roles;
                    vm.max_member = vm.currentExercice.max_member;
                    vm.currentExerciceTeams = vm.currentExercice.team;
                    vm.currentExerciceTeams.activePanel = -1;
                    vm.isTeamEmpty = false;
                    vm.currentTeamUserCreation = [];
                    verification();
                }
            });

            return vm.exercices;
        }


        function verification() {
            if (vm.currentExercice.max_member == 1) {
                var cpt = 0;
                if (vm.currentExerciceTeams.length == 0) {
                    vm.isUserAvalable = true;
                }
                else {
                    while (cpt < vm.currentExerciceTeams.length && vm.isUserAvalable != false) {
                        if (vm.currentUser.id == vm.currentExerciceTeams[cpt].team_user[0].user.id) {
                            vm.isUserAvalable = false;
                        }
                        cpt = cpt + 1;
                        console.log(vm.isUserAvalable);
                    }
                }
            }

        }

        function switchToCreateState() {

            vm.isTeamCreating = true;
            vm.currentExerciceTeam = null;
        }

        function switchToAddingState(teamId) {
            vm.isTeamCreating = false;
            vm.isAdding = true;
            vm.teamId = teamId;
        }


        function createTeamIndividu() {
            var exercice = vm.currentExercice;
            var now = moment();
            var date_end = moment(vm.currentExercice.date_end);
            if (now.isBefore(date_end)) {
                var team = new Team
                ({
                    exercice: exercice.id

                });
                Token.get(function (data) {
                    team.$save({_csrf: data._csrf}, function (response) {
                        var team = response;
                        var teamUser = new TeamUser
                        ({
                            user: vm.currentUser.id,
                            team: team.id,
                            chef: true
                        });

                        teamUser.$save({_csrf: data._csrf}, function (data) {
                            teamUser.user = findOneUser(vm.currentUser.id);
                            teamUser.team = findTeam(team.id);
                            team.team_user = [];
                            team.team_user.push(teamUser);
                            team.validation = true;
                            vm.currentExerciceTeams.push(team);
                            vm.exercices[vm.currentIndex].team = vm.currentExerciceTeams;
                            vm.isTeamEmpty = false;
                            vm.isTeamCreating = false;
                            vm.isUserAvalable = false;

                        });
                    }, function (errorResponse) {
                        //console.log("error create");
                    });
                });
            }
            else {
                vm.errorMessage = "L'exercice est déjà cloturé.";
                console.log(vm.errorMessage);
            }
        }

        function createTeamGroup() {

            var roleIndex = document.getElementById("role").selectedIndex;
            vm.isTeamCreating = false;
            var exercice = vm.currentExercice;
            var now = moment();
            var date_end = moment(vm.currentExercice.date_end);
            if (now.isBefore(date_end)) {
                var team = new Team
                ({
                    exercice: exercice.id

                });
                Token.get(function (data) {
                    team.$save({_csrf: data._csrf}, function (response) {
                        var team = response;
                        var teamUser = new TeamUser
                        ({
                            user: vm.currentUser.id,
                            team: team.id,
                            chef: true,
                            roles: vm.currentExerciceRoles[roleIndex].id
                        });
                        //console.log(teamUser);

                        teamUser.$save({_csrf: data._csrf}, function (data) {
                            teamUser.user = findOneUser(vm.currentUser.id);
                            teamUser.team = team;
                            teamUser.roles = findRole(vm.currentExerciceRoles[roleIndex].id);
                            team.team_user = [];
                            team.team_user.push(teamUser);
                            vm.currentExerciceTeams.push(team);
                            vm.exercices[vm.currentIndex].team = vm.currentExerciceTeams;
                            vm.isTeamEmpty = false;
                        });

                    }, function (errorResponse) {
                        //console.log("error create");
                    });
                });
            }
            else {
                vm.errorMessage = "L'exercice est déjà cloturé.";
                console.log(vm.errorMessage);
            }


        }

        function addMembre() {
            var roleIndex = document.getElementById("role").selectedIndex;
            //var teamId = parseInt(vm.teamNumberAdding);
            var teamId = vm.teamId;
            var canAdd = false;
            var currentTeam = findTeam(teamId);
            var email = document.getElementById('userEmail_value').value;

            var tabRoleGotten = [];
            var tabUserGotten = [];
            console.log(currentTeam.team_user);
            for (var i = 0; i < currentTeam.team_user.length; i++) {
                tabRoleGotten.push(currentTeam.team_user[i].roles.id);
                tabUserGotten.push(currentTeam.team_user[i].user.id);
            }

            var roleId = tabRoleGotten.indexOf(vm.currentExerciceRoles[roleIndex].id);


            if (_.isUndefined(currentTeam) || _.isNull(currentTeam)) {
                vm.errorMessage = "ce groupe n'existe pas ";
                console.log(vm.errorMessage);
            }
            else if (roleId != -1) {
                vm.errorMessage = "ce role est déjà pris";

                console.log(vm.errorMessage);
            }
            else {

                var cptTeam = 0;

                while (cptTeam < currentTeam.team_user.length && canAdd == false) {
                    if (currentTeam.team_user[cptTeam].chef && currentTeam.team_user[cptTeam].user.id == vm.currentUser.id) {
                        canAdd = true;
                        cptTeam = 0;
                    }
                    else {
                        vm.errorMessage = "tu n'est pas le chef de ce groupe ";
                        console.log(vm.errorMessage);
                    }
                    cptTeam = cptTeam + 1;
                }
                if (canAdd) {
                    var now = moment();
                    var date_end = moment(vm.currentExercice.date_end);

                    if (now.isBefore(date_end)) {
                        var user = findOneUserByEmail(email);
                        if (_.isUndefined(user) == false && _.isNull(user) == false) {
                            var userId = tabUserGotten.indexOf(user.id);
                            console.log(userId);
                            if (userId == -1) {
                                Token.get(function (data) {
                                    var teamUser = new TeamUser
                                    ({
                                        user: user.id,
                                        team: teamId,
                                        chef: false,
                                        roles: vm.currentExerciceRoles[roleIndex].id
                                    });

                                    teamUser.$save({_csrf: data._csrf}, function (data) {
                                        teamUser.user = findOneUser(user.id);
                                        teamUser.team = findTeam(teamId);
                                        teamUser.roles = findRole(vm.currentExerciceRoles[roleIndex].id);
                                        //console.log(teamUser);

                                        currentTeam.team_user.push(teamUser);
                                        //console.log(currentTeam);
                                        var index = _.findIndex(vm.currentExerciceTeams, { 'id': teamId });
                                        vm.currentExerciceTeams[index] = currentTeam;
                                        //console.log(vm.currentExerciceTeams[index]);
                                        vm.exercices[vm.currentIndex].team = vm.currentExerciceTeams;
                                        vm.isAdding = false;

                                        /***
                                         * Quand on ajoute un membre
                                         * user adresse = teamUser.user.email
                                         * user first name = teamUser.user.first_name
                                         * user last name = teamUser.user.lase_name
                                         * team ID = teamUser.team.id
                                         * user role = teamUser.roles.name
                                         * email chef group = currentTeam.team_user[0].user.email
                                         * exercice name = vm.currentExcercice.name
                                         * auteur email = vm.currentExcercice.author.email
                                         */
                                    });

                                });
                            }
                            else
                            {
                                vm.errorMessage = "cette étudiant est déjà dans ce groupe !";
                                console.log(vm.errorMessage);
                            }
                        }
                        else {
                            vm.errorMessage = "cette étudiant n'existe pas !";
                            console.log(vm.errorMessage);
                        }
                    }
                    else {
                        vm.errorMessage = "L'exercice est déjà cloturé.";
                        console.log(vm.errorMessage);
                    }
                }

            }
        }


        function changeCurrentExerciceDisplayed(index) {
            var exercice = vm.exercices[index];
            vm.currentExercice = angular.copy(exercice);
            //console.log(vm.currentExercice);
            vm.currentIndex = index;
            vm.currentExerciceTeams = vm.currentExercice.team;
            //console.log(vm.currentExerciceTeams);
            vm.currentExerciceTeams.activePanel = -1;
            vm.isTeamCreating = false;
            vm.currentExerciceRoles = vm.currentExercice.roles;
            vm.isUserAvalable = true;
            vm.max_member = vm.currentExercice.max_member;
            vm.isAdding = false;

            verification();


        }

        function nextExercices() {
            vm.skip = vm.skip + vm.limit;
            Token.get(function (data) {
                Exercice.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                    response.forEach(function (elem) {
                        vm.exercices.push(elem);
                    });
                }, function (errorResponse) {
                    console.log("error nextExercice");
                });
            });
        }

        function cancelCreationTeam() {
            vm.isTeamCreating = false;
            vm.currentExerciceTeam = null;
        }

        function cancelAdding() {
            vm.isAdding = false;
            vm.team = null;
        }

        function findOneUser(id) {
            var index = _.findIndex(vm.users, { 'id': id });
            vm.user = vm.users[index];
            return vm.user;
        }

        function findOneUserByEmail(email) {
            var index = _.findIndex(vm.users, { 'email': email });
            vm.user = vm.users[index];
            return vm.user;
        }


        function findRole(id) {
            var index = _.findIndex(vm.currentExerciceRoles, { 'id': id });
            vm.role = vm.currentExerciceRoles[index];
            return vm.role;
        }

        function findTeam(id) {
            var index = _.findIndex(vm.currentExerciceTeams, { 'id': id });
            vm.team = vm.currentExerciceTeams[index];
            return vm.team;
        }

        function validation(id) {
            var teamId = id;
            var currentTeam = findTeam(teamId);

            console.log(currentTeam);
            console.log(vm.currentExercice);

            /***
             * Quand on a un team nom-complet et person veux venir dans ce groupe
             * team ID = currentTeam.id
             * email chef group = currentTeam.team_user[0].user.email
             * exercice name = vm.currentExcercice.name
             * auteur email = vm.currentExcercice.author.email
             */

        }


    }
})();