(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("ExerciceCtrl", ExerciceCtrl)

        /*Filtre gerant le multiselect (ui-select)*/
        .filter('propsFilter', function () {
            return function (items, props) {
                var out = [];
                if (angular.isArray(items)) {
                    items.forEach(function (item) {
                        var itemMatches = false;

                        var keys = Object.keys(props);
                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }
                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    out = items;
                }
                return out;
            };
        });

    ExerciceCtrl.$inject = ['$scope', '$injector', 'Exercice', 'Token', 'TypeExercice', 'Role', 'TypeKit', 'User', 'toaster'];
    function ExerciceCtrl($scope, $injector, Exercice, Token, TypeExercice, Role, TypeKit, User, toaster) {
        var vm = this;

        vm.exercices = [];
        vm.isCreating = false;
        vm.isEmpty = true;
        vm.currentExercice = "";
        vm.currentUser = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;

        vm.exerciceTypes = [];
        vm.typeKits = [];
        vm.multiTypeKits = [];
        vm.multiTypeKits.selectedTypeKits = [];

        vm.roles = [];
        vm.multiRoles = [];
        vm.multiRoles.selectedRoles = [];

        /*Déclarations des fonctions*/
        vm.refresh = refresh;
        vm.getExecices = getExecices;
        vm.getRoles = getRoles;
        vm.getExerciceType = getExerciceType;
        vm.getTypeKits = getTypeKits;
        vm.getCurrentUser = getCurrentUser;

        vm.switchToCreateState = switchToCreateState;
        vm.createExercice = createExercice;
        vm.updateExercice = updateExercice;
        vm.deleteExercice = deleteExercice;
        vm.changeCurrentExerciceDisplayed = changeCurrentExerciceDisplayed;
        vm.nextExercices = nextExercices;

        vm.assignCurrentExerciceRole = assignCurrentExerciceRole;
        vm.assignCurrentExerciceType = assignCurrentExerciceType;
        vm.assignCurrentExerciceTypeKit = assignCurrentExerciceTypeKit;

        vm.checkRolesValide = checkRolesValide;
        vm.checkKitsValide = checkKitsValide;
        vm.checkIfAuthor = checkIfAuthor;
        vm.checkIfAuthorById = checkIfAuthorById;

        activate();

        function activate() {
            getCurrentUser();
            getExerciceType();
            getRoles();
            getTypeKits();
            getExecices();
        }

        function refresh() {
            vm.skip = 0;
            vm.limit = 9;
            activate();
        }

        function getCurrentUser() {
            vm.currentUser = User.logged(function (data) {
                vm.currentUser = data;
            });
            return vm.currentUser;
        }

        function getExecices() {
            vm.exercices = Exercice.getPaginateExercice({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentExercice = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                    vm.assignCurrentExerciceRole();
                    vm.assignCurrentExerciceType();
                    vm.assignCurrentExerciceTypeKit();
                }
            });
            return vm.exercices;
        }

        function getExerciceType() {
            vm.exerciceTypes = TypeExercice.getExerciceTypeOnly(function (data) {
                if (data.length == 0) {
                    console.log("Aucun type d'exercice à récuperé")
                    toaster.pop('error', "Erreur", "Aucun type d'exercice à récuperé");
                }
            });
            return vm.exerciceTypes;
        }

        function getRoles() {
            vm.roles = Role.getRolesOnly(function (data) {
                if (data.length == 0) {
                    console.log("Aucun roles à récuperé")
                    toaster.pop('error', "Erreur", "Aucun roles à récuperé");
                }
            });
            return vm.roles;
        }

        function getTypeKits() {
            vm.typeKits = TypeKit.getTypeKit(function (data) {
                if (data.length == 0) {
                    console.log("Aucun type Kit à récuperé")
                    toaster.pop('error', "Erreur", "Aucun type Kit à récuperé");

                }
            });

            return vm.typeKits;
        }

        function switchToCreateState() {
            vm.isCreating = true;
            vm.currentExercice = null;
            vm.isEmpty = false;

        }

        function createExercice() {
            var roles = angular.copy(vm.currentExercice.multiRoles);
            var typeKits = angular.copy(vm.currentExercice.multiTypeKits);
            var exercice = new Exercice({
                name: vm.currentExercice.name,
                max_member: vm.currentExercice.max_member,
                description: vm.currentExercice.description,
                type: vm.currentExercice.type,
                date_begin: vm.currentExercice.date_begin,
                date_end: vm.currentExercice.date_end,
                author: vm.currentUser,
                roles: roles,
                typeKits: typeKits
            });
            Token.get(function (data) {
                exercice.$save({_csrf: data._csrf}, function (response) {

                    vm.exercices.unshift(response);
                    var exercice = vm.exercices[0];

                    //Gere le rafraichissement des liasons directement apres la creation
                    exercice.roles = roles;
                    exercice.typeKits = typeKits;
                    exercice.author = vm.currentUser;

                    vm.currentExercice = angular.copy(exercice);
                    vm.currentIndex = 0;
                    vm.assignCurrentExerciceType();
                    vm.assignCurrentExerciceRole();
                    vm.assignCurrentExerciceTypeKit();
                    vm.isCreating = false;

                    $scope.$apply();

                }, function (errorResponse) {
                    console.log("Erreur de creation");
                    toaster.pop('error', "Erreur", "Erreur de creation");

                });
            });
        }

        function updateExercice() {
            if (vm.checkRolesValide() && vm.checkKitsValide() && vm.checkIfAuthor()) {
                var exercice = vm.currentExercice;
                exercice.roles = vm.currentExercice.multiRoles;
                exercice.typeKits = vm.currentExercice.multiTypeKits;
                Token.get(function (data) {
                    exercice.$update({_csrf: data._csrf}, function (response) {
                        vm.exercices[vm.currentIndex] = angular.copy(response);
                    }, function (errorResponse) {
                        console.log("Erreur d'update");
                        toaster.pop('error', "Erreur", "Erreur d'update");

                    });
                });
            }
            else {
                console.log("Tout les champs ne sont pas correctement remplie");
                toaster.pop('warning', "Validation d'update non effectuer");

            }

        }


        function deleteExercice(index) {
            var exercice = vm.exercices[index];
            Token.get(function (data) {
                exercice.$remove({_csrf: data._csrf}, function (response) {
                    vm.exercices.splice(index, 1);
                    if (vm.currentExercice == null || vm.currentExercice == undefined)
                        vm.currentExercice = angular.copy(vm.exercices[0]);
                    if (vm.exercices.length == 0)
                        vm.isEmpty = true;

                    switchToCreateState();
                }, function (errorResponse) {
                    console.log("Erreur de supression");
                    toaster.pop('error', "Erreur de supression");

                });
            });
        }

        function changeCurrentExerciceDisplayed(index) {
            var exercice = vm.exercices[index];
            vm.currentExercice = angular.copy(exercice);
            vm.currentIndex = index;
            vm.assignCurrentExerciceRole();
            vm.assignCurrentExerciceType();
            vm.assignCurrentExerciceTypeKit();
            vm.isCreating = false;

        }

        /*Va chercher x exercices dans la BD a partir de la derniere lignes envoyé*/
        function nextExercices() {
            vm.skip = vm.skip + vm.limit;
            Exercice.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                response.forEach(function (elem) {
                    vm.exercices.push(elem);
                });
            }, function (errorResponse) {
                console.log("Il n'y a plus d'exercice a aller chercher");
            });
        }

        /*Gere les problemes de references d'objet pour le dropdown*/
        function assignCurrentExerciceType() {
            if (!_.isNull(vm.currentExercice.type) && !_.isUndefined(vm.currentExercice.type)) {
                if (vm.currentExercice.type.name == undefined)
                    vm.currentExercice.type = vm.exerciceTypes[_.findIndex(vm.exerciceTypes, { 'id': vm.currentExercice.type })];
                else
                    vm.currentExercice.type = vm.exerciceTypes[_.findIndex(vm.exerciceTypes, { 'name': vm.currentExercice.type.name })];

            } else {
                console.log("Aucun type d'exercice");
                toaster.pop('error', "Aucun type d'exercice");

            }
        }

        /*Gere les problemes de references d'objet pour le multi select*/
        function assignCurrentExerciceRole() {

            if (_.isNull(vm.currentExercice.roles) || _.isUndefined(vm.currentExercice.roles)) {
                console.log("Aucun roles");
                toaster.pop('error', "Aucun roles");

            } else {
                vm.currentExercice.multiRoles = [];

                var idRoles = _.pluck(vm.roles, 'id');
                var idCurrentRoles = _.pluck(vm.currentExercice.roles, 'id');
                var intersection = _.intersection(idRoles, idCurrentRoles);

                intersection.forEach(function (num) {
                    vm.currentExercice.multiRoles.push(_.find(vm.roles, {'id': num}));
                });
            }
        }

        /*Gere les problemes de references d'objet pour le multi select*/
        function assignCurrentExerciceTypeKit() {

            if (_.isNull(vm.currentExercice.typeKits) || _.isUndefined(vm.currentExercice.typeKits)) {
                console.log("Aucun type kits");
                toaster.pop('error', "Aucun type kits");

            } else {
                vm.currentExercice.multiTypeKits = [];

                var idTypeKits = _.pluck(vm.typeKits, 'id');
                var idCurrentTypeKits = _.pluck(vm.currentExercice.typeKits, 'id');
                var intersection = _.intersection(idTypeKits, idCurrentTypeKits);

                intersection.forEach(function (num) {
                    vm.currentExercice.multiTypeKits.push(_.find(vm.typeKits, {'id': num}));
                });
            }
        }

        /*Verifie que aucun role n'a été supprimé*/
        function checkRolesValide() {
            var newRoles = _.pluck(vm.currentExercice.multiRoles, 'name');
            var oldRoles = _.pluck(_.find(vm.exercices, {'id': vm.currentExercice.id}).roles, 'name');

            if (_.isEqual(oldRoles, _.intersection(oldRoles, newRoles))) {
                return true;
            }
            else {
                console.log("Changement role non valide");
                toaster.pop('error', "Changement role non valide");

                return false;
            }
        }

        /*Verifie que aucun type kit n'a été supprimé*/
        function checkKitsValide() {
            var newTypeKits = _.pluck(vm.currentExercice.multiTypeKits, 'name');
            var oldTypeKits = _.pluck(_.find(vm.exercices, {'id': vm.currentExercice.id}).typeKits, 'name');

            if (_.isEqual(oldTypeKits, _.intersection(oldTypeKits, newTypeKits)))
                return true;
            else {
                console.log("Changement kit non valide");
                toaster.pop('error', "Changement kit non valide");

                return false;
            }
        }

        /*Verifie que l'user qui veux update est bien l'autheur*/
        function checkIfAuthor() {
            if (vm.currentExercice != null) {
                if (vm.currentExercice.author != undefined && vm.currentUser.id == vm.currentExercice.author.id) {
                    return true;
                }
                else if (vm.currentUser.userlevel[0].name === "admin") {
                    console.log("User is admin");
                    return true;
                }
                else {
                    console.log("User is not admin");
                    console.log("User is not author");
                    return false;
                }

            }
        }

        function checkIfAuthorById(id) {
            if (vm.currentUser.id == id) {
                return true;
            }
            else if (vm.currentUser.userlevel[0].name === "admin") {
                console.log("User is admin");
                return true;
            }
            else {
                console.log("User is not admin");
                console.log("User is not author");
                return false;
            }
        }
    }
})();
