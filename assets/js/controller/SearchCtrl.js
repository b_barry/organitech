﻿(function () {
  'use strict';
  angular
    .module('organitech')
    .controller("SearchCtrl", SearchCtrl);

  SearchCtrl.$inject = ['$injector', 'Token', 'User', 'Team', '$templateCache'];


  function SearchCtrl($injector,Token, User, Team, $templateCache) {
    /* jshint validthis: true */
    var vm = this;

    vm.users = [];
    vm.teams = [];
    vm.currentUser = {};
    vm.currentTeams = [];
    vm.isUserLoaded = false;
    vm.isTeamsLoaded = false;
    vm.isEmptyUsers = true;
    vm.isEmptyTeams = true;
    vm.isUserInTeam = false;
    vm.isLoadedFirst = false;
    vm.moyenneCurrentUser = 0;
    vm.moyennePonderation = 0;
    vm.currentIndex = 0;
    vm.page = 1;
    vm.limit = 10;

    vm.isEditing = false;



    vm.getUsers = getUsers;
    vm.getTeams = getTeams;
    vm.getCurrentTeams = getCurrentTeams;

    vm.changeCurrentUserDisplayed = changeCurrentUserDisplayed;




    activate();

    function activate() {
      getUsers();
      getTeams();
    }


    function getUsers() {
      vm.users = [];
      vm.users = User.query(function (data) {
        if (data.length != 0) {
          vm.isUserLoaded = true;
          vm.isEmptyUsers = false;
        }
      });
      return vm.users;
    }

    function getTeams() {
      vm.teams = [];
      vm.teams = Team.query(function (data) {
        if (data.length != 0) {
          vm.isTeamsLoaded = true;
          vm.isEmptyTeams = false;
        }
      });
      return vm.teams;
    }

    function changeCurrentUserDisplayed(userId) {
      vm.currentUser ={};
      vm.isLoadedFirst = true;
      vm.isUserInTeam = false;
      _.forEach(vm.users, function (usersearch){
        if(usersearch.id == userId){
          vm.currentUser = angular.copy(usersearch);
        }
      });
      getCurrentTeams();
    }

    function getCurrentTeams() {
      vm.currentTeams = [];
      vm.moyenneCurrentUser = 0;
      vm.moyennePonderation = 0;
      _.forEach(vm.currentUser.team_user, function (team_user) {
        _.forEach(vm.teams, function (team) {

          if (team_user.team == team.id && team.exercice !=null) {
            _.forEach(team.evaluation, function (evals) {
              vm.moyenneCurrentUser = vm.moyenneCurrentUser + parseFloat(evals.mark);
              vm.moyennePonderation = vm.moyennePonderation + evals.ponderation;
            });
            vm.currentTeams.push(team);

            vm.isUserInTeam = true;

          }

        });

      });
    }

    function nextExercices() {
      vm.page++;
      /*Token.get(function (data) {
       Exercice.paginate({limit: vm.limit, page: vm.page}, function (response) {
       if (response.length > 0) {
       response.forEach(function (elem) {
       vm.exercices.push(elem);
       });
       }
       }, function (errorResponse) {
       console.log("error nextExercice");
       });
       });*/
    }
  }
})();
