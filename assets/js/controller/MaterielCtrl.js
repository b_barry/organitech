(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("MaterielCtrl", MaterielCtrl);

    MaterielCtrl.$inject = ['$injector', 'Materiel', 'Token', 'toaster'];


    function MaterielCtrl($injector, Materiel, Token, toaster) {
        /* jshint validthis: true */
        var vm = this;

        vm.materiels = [];
        vm.isCreating = false;
        vm.isEmpty = true;

        vm.currentMateriel = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;

        vm.getMateriels = getMateriels;
        vm.switchToCreateState = switchToCreateState;
        vm.createMateriel = createMateriel;
        vm.updateMateriel = updateMateriel;
        vm.deleteMateriel = deleteMateriel;
        vm.changeCurrentMaterielDisplayed = changeCurrentMaterielDisplayed;
        vm.nextMateriel = nextMateriel;

        activate();

        function activate() {
            getMateriels();
        }

        function getMateriels() {
            vm.materiels = Materiel.getPaginate({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentMateriel = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                }
                vm.isLoading = false;
            });
            return vm.materiels;
        }

        function switchToCreateState() {
            vm.isCreating = true;
            vm.currentMateriel = null;
            vm.isEmpty = false;
        }

        function createMateriel() {
            var materiel = new Materiel({
                nom: vm.currentMateriel.nom,
                picture: vm.currentMateriel.picture,
                instructionManuel: vm.currentMateriel.instructionManuel
            });
            Token.get(function (data) {
                materiel.$save({_csrf: data._csrf}, function (response) {

                    vm.materiels.unshift(response);
                    vm.currentMateriel = angular.copy(response);
                    vm.currentIndex = 0;

                    vm.isCreating = false;
                }, function (errorResponse) {
                    console.log("error create");
                    toaster.pop('error', "Erreur","Erreur de creation");

                });
            });
        }

        function updateMateriel() {
            var materiel = vm.currentMateriel;
            Token.get(function (data) {
                materiel.$update({_csrf: data._csrf}, function (response) {
                    vm.materiels[vm.currentIndex] = angular.copy(response);
                }, function (errorResponse) {
                    console.log("error update");
                    toaster.pop('error', "Erreur","Erreur d'update");
                });
            });
        }


        function deleteMateriel(index) {
            var materiel = vm.materiels[index];
            Token.get(function (data) {
                vm.currentMateriel.$remove({_csrf: data._csrf}, function (response) {
                    vm.materiels.splice(index, 1);
                    if (vm.currentMateriel == null || vm.currentMateriel == undefined)
                        vm.currentMateriel = angular.copy(vm.materiels[0]);
                    if (vm.materiels.length == 0)
                        vm.isEmpty = true;

                    switchToCreateState();
                }, function (errorResponse) {
                    console.log("error delete");
                    toaster.pop('error', "Erreur de supression");
                });
            });
        }

        function changeCurrentMaterielDisplayed(index) {
            var materiel = vm.materiels[index];
            vm.currentMateriel = angular.copy(materiel);
            vm.currentIndex = index;
            vm.isCreating = false;
        }

        function nextMateriel() {
            vm.skip = vm.skip + vm.limit;
            Materiel.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                response.forEach(function (elem) {
                    vm.materiels.push(elem);
                });
            }, function (errorResponse) {
                console.log("error nextMateriel");
            });
        }
    }
})();