(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("ModalManageUserRoleCtrl", ModalManageUserRoleCtrl);

    ModalManageUserRoleCtrl.$inject = ['$modalInstance', 'user', 'userLevels', 'codeConfirmationChangeRoleUser', 'Token', 'User', 'Mailer'];

    function ModalManageUserRoleCtrl($modalInstance, user, userLevels, codeConfirmationChangeRoleUser, Token, User, Mailer) {

        /* jshint validthis: true */
        var vm = this;
        vm.currentUser = user;
        vm.userLevels = userLevels;
        vm.codeConfirmationChangeRoleUser = codeConfirmationChangeRoleUser;

        vm.selectedUserLevel = userLevels[1];
        vm.codeChangeRoleUser = '';
        vm.errorValidationChangeRoleUser = false;

        vm.saveChangeRoleUser = saveChangeRoleUser;
        vm.cancelChangeRoleUser = cancelChangeRoleUser;
        vm.translateUserLevel = translateUserLevel;


        activate();

        function activate() {
        }

        function translateUserLevel(name) {
            if (name == 'admin') {
                return 'Administrateur';
            }

            if (name == 'teacher') {
                return 'Professeur';
            }

            if (name == 'student') {
                return 'Etudiant';
            }
        }

        function saveChangeRoleUser() {
            var code = vm.codeChangeRoleUser;

            if (code == vm.codeConfirmationChangeRoleUser) {
                User.lock({userId: vm.currentUser.id}, function (isLocked) {
                    if (isLocked) {
                        vm.errorValidationChangeRoleUser = false;
                        vm.currentUser.userlevel = vm.selectedUserLevel;
                        Token.get(function (data) {
                            vm.currentUser.$update({_csrf: data._csrf}, function (data) {
                                if (data.length != 0) {
                                    Mailer.sendUpdatedUserRoleByAdmin({_csrf: data._csrf,
                                        userAddress: user.email,
                                        username: user.first_name + " " + user.last_name,
                                        role: translateUserLevel(vm.selectedUserLevel.name)});

                                    User.unlock({userId: vm.currentUser.id}, function (isUnlocked) {
                                        $modalInstance.close(data);
                                    });

                                }
                                else {
                                    User.unlock({userId: vm.currentUser.id}, function (isUnlocked) {
                                        $modalInstance.dismiss("Une erreur s'est produite, impossible de changer le niveau d'acces");
                                    });
                                }


                            });
                        });
                    }
                    else {
                        $modalInstance.dismiss("Un admin est deja occupé à update ce user, veuillez ressayer ds quelque minute");
                    }
                });


            }
            else {
                vm.errorValidationChangeRoleUser = true;
            }
        }

        function cancelChangeRoleUser() {
            $modalInstance.dismiss("Cancel");

        }

    }
})();