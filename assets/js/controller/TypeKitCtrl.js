(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("TypeKitCtrl", TypeKitCtrl);

    TypeKitCtrl.$inject = ['$injector', 'TypeKit', 'Token', 'Materiel', 'toaster'];


    function TypeKitCtrl($injector, TypeKit, Token, Materiel, toaster) {
        /* jshint validthis: true */
        var vm = this;

        vm.typeKits = [];
        vm.materiels = [];
        vm.materielCheck = new Array();
        vm.isCreating = false;
        vm.isEmpty = true;
        vm.currentTypeKit = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;

        vm.refresh = refresh;
        vm.getTypeKits = getTypeKits;
        vm.getMateriels = getMateriels;
        vm.switchToCreateState = switchToCreateState;
        vm.createTypeKit = createTypeKit;
        vm.updateTypeKit = updateTypeKit;
        vm.deleteTypeKit = deleteTypeKit;
        vm.changeCurrentTypeKitDisplayed = changeCurrentTypeKitDisplayed;
        vm.nextTypeKits = nextTypeKits;
        vm.toggleSelection = toggleSelection;
        vm.clearCheckBox = clearCheckBox;
        vm.checkIfChecked = checkIfChecked;
        vm.resetCheckBox = resetCheckBox;

        activate();

        function activate() {
            getTypeKits();
            getMateriels();
        }

        function refresh() {
            vm.skip = 0;
            vm.limit = 9;
            activate();
        }

        function getTypeKits() {
            vm.typeKits = TypeKit.getPaginate({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentTypeKit = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                    changeCurrentTypeKitDisplayed(0);
                }
            });
            return vm.typeKits;
        }

        function getMateriels() {
            vm.materiels = Materiel.getMaterielOnly(function (data) {
                if (data.length == 0) {
                    console.log("Aucun Materiel récuperé");
                    toaster.pop('error', "Aucun Materiel récuperé");
                }
            });

            return vm.materiels;
        }

        function switchToCreateState() {
            vm.resetCheckBox();
            vm.isCreating = true;
            vm.currentTypeKit = null;
            vm.isEmpty = false;
        }

        function createTypeKit() {
            var typeKit = new TypeKit({
                name: vm.currentTypeKit.name,
                materiel: vm.materielCheck
            });

            Token.get(function (data) {
                typeKit.$save({_csrf: data._csrf}, function (response) {
                    vm.typeKits.unshift(response);
                    vm.currentTypeKit = angular.copy(response);
                    clearCheckBox();
                    vm.currentIndex = 0;
                    vm.isCreating = false;
                }, function (errorResponse) {
                    toaster.pop('error', "Erreur","Erreur de creation");
                    console.log("error create");
                });
            });
        }

        function updateTypeKit() {
            var typeKit = vm.currentTypeKit;
            typeKit.materiel = vm.materielCheck;

            Token.get(function (data) {
                typeKit.$update({_csrf: data._csrf}, function (response) {
                    vm.typeKits[vm.currentIndex] = angular.copy(response);
                }, function (errorResponse) {
                    toaster.pop('error', "Erreur","Erreur d'update");
                    console.log("error update");
                });
            });
        }


        function deleteTypeKit(index) {
            var typeKit = vm.typeKits[index];
            Token.get(function (data) {
                typeKit.$remove({_csrf: data._csrf}, function (response) {
                    vm.typeKits.splice(index, 1);
                    if (vm.currentTypeKit == null || vm.currentTypeKit == undefined)
                        vm.currentTypeKit = angular.copy(vm.typeKits[0]);
                    if (vm.typeKits.length == 0)
                        vm.isEmpty = true;

                    switchToCreateState();

                }, function (errorResponse) {
                    console.log("error delete");
                    toaster.pop('error', "Erreur de supression");

                });
            });
        }

        function changeCurrentTypeKitDisplayed(index) {
            var typeKit = vm.typeKits[index];
            vm.currentTypeKit = angular.copy(typeKit);
            vm.currentIndex = index;
            vm.isCreating = false;
            vm.clearCheckBox();
        }

        function nextTypeKits() {
            vm.skip = vm.skip + vm.limit;
            TypeKit.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                response.forEach(function (elem) {
                    vm.typeKits.push(elem);
                });
            }, function (errorResponse) {
                console.log("error nextTypeKit");
            });
        }

        function toggleSelection(mat) {
            var index = _.findIndex(vm.materielCheck, {id: mat.id});
            if (index > -1) {
                vm.materielCheck.splice(index, 1);
            }
            else {
                vm.materielCheck.push(mat);
            }
        }

        function clearCheckBox() {
            vm.materielCheck = vm.currentTypeKit.materiel;
        }

        function resetCheckBox(){
            vm.materielCheck = "";
        }

        function checkIfChecked(mat) {
            if (_.where(vm.materielCheck, {id: mat.id}).length === 0) {
                return false;
            }
            else {
                return true;
            }
        }
    }
})();