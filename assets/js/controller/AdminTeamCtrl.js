(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("AdminTeamCtrl", AdminTeamCtrl);

    AdminTeamCtrl.$inject = ['Exercice', 'Token', 'User', 'Team', 'TeamUser','toaster', '$templateCache'];


    function AdminTeamCtrl(Exercice, Token, User, Team, TeamUser,toaster, $templateCache) {
        /* jshint validthis: true */
        var vm = this;

        vm.exercices = [];
        vm.users = [];
        vm.isUserLoaded = false;
        vm.isEmpty = true;
        vm.currentExercice = "";
        vm.currentIndex = 0;
        vm.page = 1;
        vm.limit = 10;


        vm.isEditing = false;
        vm.isDeletingUserInTeam = false;
        vm.isMovingUserFromTeamToOtherTeam = false;
        vm.currentTeams = [];

        vm.refresh = refresh;
        vm.getExecices = getExecices;
        vm.getUsers = getUsers;

        vm.changeCurrentExerciceDisplayed = changeCurrentExerciceDisplayed;
        vm.switchEditing = switchEditing;
        vm.switchDeletingUserInTeam = switchDeletingUserInTeam;
        vm.switchMovingUserFromTeamToOtherTeam = switchMovingUserFromTeamToOtherTeam;
        vm.resetAllState = resetAllState;

        vm.validateTeam = validateTeam;
        vm.isComplete = isComplete
        vm.deleteTeam = deleteTeam;
        vm.deleteUserInTeam = deleteUserInTeam;
        vm.nextExercices = nextExercices;


        activate();

        function activate() {
            //getUsers();
            getExecices();
        }

        function refresh() {
            vm.page = 1;
            vm.limit = 10;
            activate();
        }

        function getExecices() {
            vm.exercices = Exercice.getWithTeamPopulatedPaginate({limit: vm.limit, page: vm.page}, function (data) {
                if (data.length != 0) {
                    vm.currentExercice = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.currentTeams = vm.currentExercice.team;
                    vm.currentTeams.activePanel = -1;
                    vm.isEmpty = false;
                }
            });
            return vm.exercices;
        }

        function getUsers() {
            vm.users = User.query(function (data) {
                if (data.length != 0) {
                    vm.isUserLoaded = true;
                }
            });
            return vm.users;
        }

        function changeCurrentExerciceDisplayed(index) {
            var exercice = vm.exercices[index];
            vm.currentExercice = angular.copy(exercice);
            vm.currentIndex = index;

            vm.currentTeams = vm.currentExercice.team;
            vm.currentTeams.activePanel = -1;
            vm.isEditing = false;
            vm.isDeletingUserInTeam = false;
            vm.isMovingUserFromTeamToOtherTeam = false;
            vm.selectedTeamToDeleteMember = null;
        }

        function switchEditing() {
            vm.isEditing = true;
        }

        function switchDeletingUserInTeam() {
            vm.isDeletingUserInTeam = true;
            vm.isMovingUserFromTeamToOtherTeam = false;

        }

        function switchMovingUserFromTeamToOtherTeam() {
            vm.isMovingUserFromTeamToOtherTeam = true;
            vm.isDeletingUserInTeam = false;

        }

        function resetAllState() {
            vm.isEditing = false;
            vm.isDeletingUserInTeam = false;
            vm.isMovingUserFromTeamToOtherTeam = false;

        }

        function validateTeam(idTeam) {
            var team =new Team({
                id: vm.currentTeams[idTeam].id,
                validate:vm.currentTeams[idTeam].validate
            });
            if (team.validate == false) {
                team.validate = true;
                Token.get(function (data) {
                    team.$update({_csrf: data._csrf}, function (data) {
                        if (data.length != 0) {
                            toaster.pop('success', "Activation de la Team n° "+team.id);

                            vm.currentTeams[idTeam].validate = true;

                        }
                        else {
                            toaster.pop('error', "Erreur","La team n'a pas été mise à jour");
                        }

                    });
                });
            }
            else {
                team.validate = false;
                Token.get(function (data) {
                    team.$update({_csrf: data._csrf}, function (data) {
                        if (data.length != 0) {
                            toaster.pop('success', "Desactivation de la Team n°"+team.id);

                            vm.currentTeams[idTeam].validate = false;
                        }
                        else {
                            toaster.pop('error', "Erreur","La team n'a pas été mise à jour");
                        }
                    });
                });
            }

        }

        function isComplete(idTeam) {
            var nbrUserInTeam = vm.currentTeams[idTeam].team_user.length;
            if (nbrUserInTeam == vm.currentExercice.max_member) {
                return true;
            }
            return false;


        }

        function deleteTeam(idTeam) {
            var team = new Team({
                id: vm.currentTeams[idTeam].id
            });

            Token.get(function (data) {
                team.$remove({_csrf: data._csrf}, function (response) {
                    toaster.pop('success', "La Team n°"+team.id+" a bien été supprimé");
                    vm.currentTeams.splice(idTeam, 1);

                }, function (errorResponse) {
                    toaster.pop('error', "La Team n°"+team.id+" n'a pas été supprimé");
                });
            });

        }

        function deleteUserInTeam(idTeam, idTeamUser) {
            var teamUser = new TeamUser({
                id: vm.currentTeams[idTeam].team_user[idTeamUser].id
            });

            Token.get(function (data) {
                teamUser.$remove({_csrf: data._csrf}, function (response) {
                    toaster.pop('success', "L'étudiant dans la team n°"+team.id+" a bien été supprimé");
                    vm.currentTeams[idTeam].team_user.splice(idTeamUser, 1);
                    console.log(response);

                }, function (errorResponse) {
                    toaster.pop('error', "L'étudiant dans la team n°"+team.id+" n'a pas été supprimé");
                });
            });

        }


        function nextExercices() {
            vm.page++;
            /*Token.get(function (data) {
             Exercice.paginate({limit: vm.limit, page: vm.page}, function (response) {
             if (response.length > 0) {
             response.forEach(function (elem) {
             vm.exercices.push(elem);
             });
             }
             }, function (errorResponse) {
             console.log("error nextExercice");
             });
             });*/
        }
    }
})();