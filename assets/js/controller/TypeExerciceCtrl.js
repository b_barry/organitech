(function () {
    'use strict';
    angular
        .module('organitech')
        .controller("TypeExerciceCtrl", TypeExerciceCtrl);

    TypeExerciceCtrl.$inject = ['$injector', 'TypeExercice', 'Token', 'toaster'];


    function TypeExerciceCtrl($injector, TypeExercice, Token, Toaster) {
        /* jshint validthis: true */
        var vm = this;

        vm.typeExercices = [];
        vm.isCreating = false;
        vm.isEmpty = true;
        vm.currentTypeExercice = "";
        vm.currentIndex = 0;
        vm.skip = 0;
        vm.limit = 9;
        vm.couleurs = ['Violet', 'Rouge', 'Vert', 'Bleu', 'Blanc', 'Jaune'];

        vm.refresh = refresh;
        vm.getTypeExercices = getTypeExercices;
        vm.switchToCreateState = switchToCreateState;
        vm.createTypeExercice = createTypeExercice;
        vm.updateTypeExercice = updateTypeExercice;
        vm.deleteTypeExercice = deleteTypeExercice;
        vm.changeCurrentTypeExerciceDisplayed = changeCurrentTypeExerciceDisplayed;
        vm.nextTypeExercices = nextTypeExercices;

        activate();

        function activate() {
            getTypeExercices();
        }

        function refresh() {
            vm.skip = 0;
            vm.limit = 9;
            activate();
        }

        function getTypeExercices() {
            vm.typeExercices = TypeExercice.getPaginate({limit: vm.limit, skip: vm.skip}, function (data) {
                if (data.length != 0) {
                    vm.currentTypeExercice = angular.copy(data[0]);
                    vm.currentIndex = 0;
                    vm.isEmpty = false;
                }
            });
            return vm.typeExercices;
        }

        function switchToCreateState() {
            vm.isCreating = true;
            vm.currentTypeExercice = null;
            vm.isEmpty = false;
        }

        function createTypeExercice() {
            console.log(vm.currentTypeExercice.color);

            var typeExercice = new TypeExercice({
                name: vm.currentTypeExercice.name,
                couleur: vm.currentTypeExercice.couleur
            });

            console.log(typeExercice);

            Token.get(function (data) {
                typeExercice.$save({_csrf: data._csrf}, function (response) {

                    vm.typeExercices.unshift(response);
                    vm.currentTypeExercice = angular.copy(response);
                    vm.currentIndex = 0;

                    vm.isCreating = false;
                }, function (errorResponse) {
                    console.log("error create");
                    toaster.pop('error', "Erreur","Erreur de creation");

                });
            });
        }

        function updateTypeExercice() {
            var typeExercice = vm.currentTypeExercice;
            Token.get(function (data) {
                typeExercice.$update({_csrf: data._csrf}, function (response) {
                    vm.typeExercices[vm.currentIndex] = angular.copy(response);
                }, function (errorResponse) {
                    console.log("error update");
                    toaster.pop('error', "Erreur","Erreur d'update");

                });
            });
        }


        function deleteTypeExercice(index) {
            var typeExercice = vm.typeExercices[index];
            Token.get(function (data) {
                typeExercice.$remove({_csrf: data._csrf}, function (response) {
                    vm.typeExercices.splice(index, 1);
                    if (vm.currentTypeExercice == null || vm.currentTypeExercice == undefined)
                        vm.currentTypeExercice = angular.copy(vm.typeExercices[0]);
                    if (vm.typeExercices.length == 0)
                        vm.isEmpty = true;

                    switchToCreateState();
                }, function (errorResponse) {
                    console.log("error delete");
                    toaster.pop('error', "Erreur de supression");
                });
            });
        }

        function changeCurrentTypeExerciceDisplayed(index) {
            var typeExercice = vm.typeExercices[index];
            vm.currentTypeExercice = angular.copy(typeExercice);
            vm.currentIndex = index;
            vm.isCreating = false;
        }

        function nextTypeExercices() {
            vm.skip = vm.skip + vm.limit;
            console.log(vm.skip);
            TypeExercice.paginate({limit: vm.limit, skip: vm.skip}, function (response) {
                console.log(response);
                response.forEach(function (elem) {
                    vm.typeExercices.push(elem);
                });
            }, function (errorResponse) {
                console.log("error nextTypeExercice");
            });
        }
    }
})();