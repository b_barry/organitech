(function() {
    angular.module('validation.rule', ['validation'])
        .config(['$validationProvider',
            function($validationProvider) {

                var expression = {
                    required: function(value) {
                        return !!value;
                    },
                    date: /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})(\.000Z)/,
                    number: /^\d+$/
                };

                var defaultMsg = {
                    required: {
                        error: 'Ce champ est obligatoire!',
                        success: '✔'
                    },
                    date: {
                        error: 'Ce champ doit etre une date valide en JJ/MM/AAAA',
                        success: '✔'
                    },
                    number: {
                        error: 'Ce champ doit etre numerique',
                        success: '✔'
                    }
                };

                $validationProvider.setExpression(expression).setDefaultMsg(defaultMsg);

            }
        ]);

}).call(this);
