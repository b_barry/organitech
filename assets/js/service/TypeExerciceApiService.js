(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("TypeExercice", TypeExercice);

    TypeExercice.$inject = ['$resource'];

    function TypeExercice($resource, csrf) {
        return $resource('/TypeExercices/:typeExerciceId?:filter', {
            typeExerciceId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getExerciceTypeOnly: {
                method: 'GET',
                url: '/TypeExercices/getExerciceTypeOnly',
                isArray: true
            }
        });
    }
})();