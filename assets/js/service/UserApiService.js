﻿(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("User", User);

    User.$inject = ['$resource'];

    function User($resource, csrf) {
        return $resource('/users/:userId?:filter', {
            userId: '@id',
            filter: 'sort=updatedAt DESC'
        }, {
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            getAllWithoutCurentLogged:{
                method: 'GET',
                isArray: true,
                url: '/users/getAllWithoutCurentLogged'

            },
            logged: {
                method: 'GET',
                isArray: false,
                url: '/users/logged'

            },
            lock:{
                method: 'GET',
                isArray: false,
                url: '/users/:userId/lock',
                userId: '@id'
            },
            unlock: {
                method: 'GET',
                isArray: false,
                url: '/users/:userId/unlock',
                userId: '@id'
            },
            getPaginateUser: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            }
        });
    }

})();
