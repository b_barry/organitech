(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Evaluation", Evaluation);

    Evaluation.$inject = ['$resource'];

    function Evaluation($resource, csrf) {
        return $resource('/evaluations/:evaluationId?:filter', {
            evaluationId: '@id',
            filter: 'sort=updatedAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            }
        });
    }
})();
