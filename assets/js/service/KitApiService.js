(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Kit", Kit);

    Kit.$inject = ['$resource'];

    function Kit($resource, csrf) {
        return $resource('/kits/:kitId?:filter', {
            kitId: '@id',
            filter: 'sort=updatedAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getKitsOnly: {
                method: 'GET',
                url: '/Kits/getKitsOnly',
                isArray: true
            }
        });
    }
})();
