(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Token", Token);

    Token.$inject = ['$resource'];

    function Token($resource) {
        return $resource('/csrfToken', {
            get: {
                method: 'GET',
                isArray: false
            }
        });
    }
})();