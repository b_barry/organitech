(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Materiel", Materiel);

    Materiel.$inject = ['$resource'];

    function Materiel($resource, csrf) {
        return $resource('/materiels/:materielId?:filter', {
            materielId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getMaterielOnly: {
                method: 'GET',
                url: '/Materiels/getMaterielOnly',
                isArray: true
            }
        });
    }
})();