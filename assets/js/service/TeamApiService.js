
(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Team", Team);

    Team.$inject = ['$resource'];

    function Team($resource, csrf) {
        return $resource('/teams/:teamId?:filter', {
            teamId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            }
        });
    }


})();