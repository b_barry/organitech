(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Exercice", Exercice);

    Exercice.$inject = ['$resource'];

    function Exercice($resource, csrf) {
        return $resource('/exercices/:exerciceId?:filter', {
            exerciceId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginateExercice: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getWithTeamPopulatedPaginate: {
                method: 'GET',
                isArray: true,
                url: '/exercices/indexWithTeamPopulatedPaginate',
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getExerciceFormattedCalendar: {
                method: 'GET',
                isArray: true,
                url: '/exercices/calendar'

            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            }
        });

    }
})();
