(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Mailer", Mailer);

    Mailer.$inject = ['$resource'];

    function Mailer($resource, csrf) {
        return $resource('/mailers/:mailerId?:filter', {
            mailerId: '@id',
            filter: 'sort=updatedAt DESC'
        }, {
            sendActivationUserByAdmin: {
                method: 'GET',
                url: '/mailers/sendActivationUserByAdmin'
            },
            sendDeactivationUserByAdmin: {
                method: 'GET',
                url: '/mailers/sendDeactivationUserByAdmin'
            },
            sendUpdatedUserRoleByAdmin: {
                method: 'GET',
                url: '/mailers/sendUpdatedUserRoleByAdmin'
            },
            sendTeamAdded: {
                method: 'GET',
                url: '/mailers/sendTeamAdded'
            }
        });
    }
})();
