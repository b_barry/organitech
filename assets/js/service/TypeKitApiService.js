(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("TypeKit", TypeKit);

    TypeKit.$inject = ['$resource'];

    function TypeKit($resource, csrf) {
        return $resource('/TypeKits/:typeKitId?:filter', {
            typeKitId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getTypeKit: {
                method: 'GET',
                isArray: true,
                url: '/TypeKits/getTypeKit'
            }
        });
    }
})();
