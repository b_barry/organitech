(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Role", Role);

    Role.$inject = ['$resource'];

    function Role($resource, csrf) {
        return $resource('/roles/:roleId?:filter', {
            roleId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getRolesOnly: {
                method: 'GET',
                url: '/Roles/getRolesOnly',
                isArray: true
            }
        });
    }
})();