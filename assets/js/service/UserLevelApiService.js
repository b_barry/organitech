(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("UserLevel", UserLevel);

    UserLevel.$inject = ['$resource'];

    function UserLevel($resource) {
        return $resource('/UserLevels/:userLevelId', {
            userLevelId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
})();