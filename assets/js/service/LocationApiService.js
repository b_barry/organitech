(function () {
    'use strict';
    angular
        .module('organitech')
        .factory("Location", Location);

    Location.$inject = ['$resource'];

    function Location($resource, csrf) {
        return $resource('/locations/:locationId?:filter', {
            locationId: '@id',
            filter: 'sort=createdAt DESC'
        }, {
            save: {
                method: 'POST',
                headers: {
                    _csrf: csrf
                }
            },
            update: {
                method: 'PUT',
                headers: {
                    _csrf: csrf
                }
            },
            remove: {
                method: 'DELETE',
                headers: {
                    _csrf: csrf
                }
            },
            getPaginateLocation: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            },
            getExerciceFormattedCalendar: {
                method: 'GET',
                isArray: true,
                url: '/locations/calendar'

            },
            paginate: {
                method: 'GET',
                isArray: true,
                params: {
                    filter: 'sort=createdAt DESC'
                }
            }
        });
    }
})();
