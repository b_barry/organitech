$(document).ready(function(){
    $.validator.addMethod("emailHelb", function(value, element) {
        var mail = value;
        var position = mail.toLowerCase().indexOf("@helb-prigogine.be");
        var bool = false;
        if(position != -1) bool = true;
        return this.optional(element) || bool;
    }, 'L\'adresse email doit etre une adresse \"@helb-prigogine.be\" valide');

    $('#form-signup').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true,
                emailHelb : true
            },

            confMail: { /* name */
                required: true,
                email: true,
                equalTo: "#email" /* id */
            },
            password: {
                required: true,
                minlength: 6
            },
            confPassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            success: function(element) {
                element.text('Champ valide').addClass('valid')
            }
        }
    });

    $('#form-forgotPassword').validate({
        rules: {

            email: {
                required: true,
                email: true,
                emailHelb : true
            },

            confMail: { /* name */
                required: true,
                email: true,
                equalTo: "#email" /* id */
            },
            success: function(element) {
                element.text('Champ valide').addClass('valid')
            }
        }
    });
});