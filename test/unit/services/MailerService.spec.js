/**
 * Created by Boubacar on 14-10-14.
 */

var MailerService = require('../../../api/services/MailerService');


describe('The Service MailerService', function () {
    this.timeout(15000);
    describe('when we invoke the sendActivationAccount method', function () {

        it('should send email activation', function (done) {
            MailerService.sendActivationAccount("sidbouba@gmail.com", "boubasididi", done);
        });
    }),
        describe('when we invoke the sendActivationAccountTeacher method', function () {

            it('should send email activation for a teacher', function (done) {
                MailerService.sendActivationAccountTeacher(done);
            });
        }),

        describe('when we invoke the sendTeamAdded method', function () {

            it('should send email activation when a student add a other student in his team', function (done) {
                MailerService.sendTeamAdded("sidbouba@gmail.com", done);
            });
        }),
        describe('when we invoke the sendEmailPasswordReset method', function () {

            it('should send email password reset', function (done) {
                MailerService.sendEmailPasswordReset("sidbouba@gmail.com", "boubasididi", done);
            });
        })

});