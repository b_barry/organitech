/**
 * Created by Boubacar on 13-10-14.
 */
'use strict'

var assert=require('assert'),
    uuid=require('node-uuid'),
    ToolsService=require('../../../api/services/ToolsService');

describe('The Service TokenService',function(){

    describe('when we invoke the getTokenActivationUser method',function(){

        it('should generate a different uuid token',function(){
            assert.notEqual(ToolsService.getTokenActivationUser(),ToolsService.getTokenActivationUser());
        });
    }),
        describe('when we invoke the getTokenForgotPassword method',function(){

            it('should generate a different uuid token',function(){
                assert.notEqual(ToolsService.getTokenForgotPassword(),ToolsService.getTokenForgotPassword());
            });
        }),
        describe('when we invoke the generateHash method',function(){

            it('should generate a different hashed password',function(){
                assert.notEqual('password',ToolsService.generateHash('password'));
            });
        }),
        describe('when we invoke the compareHash method',function(){

            it('should verify if the data and hash is the same',function(){
                assert.ok(ToolsService.compareHash('password',ToolsService.generateHash('password')));
            });
        });
});